import sys
import os
import numpy as np
import cv2
from random import shuffle
from keras.models import Sequential, Model
from keras.layers.convolutional import Conv3D, Conv2D, UpSampling2D
from keras.layers.convolutional_recurrent import ConvLSTM2D
from keras.layers.normalization import BatchNormalization
from keras.layers.recurrent import LSTM
from keras.layers import Dense, Flatten, TimeDistributed, Dropout, MaxPooling2D, Input, Reshape, \
                         Concatenate, Lambda, AveragePooling2D, ZeroPadding2D

from keras.optimizers import adadelta
from keras.applications.vgg16 import VGG16
from keras.optimizers import Optimizer
import keras.backend as K
import tensorflow as tf



# Function to generate a list of files to be processed
def generate_movies_from_list(n_samples=100, n_frames=9, channel_number=1, stage='train'):

    row = 45
    col = 45
    x = np.zeros((n_samples, n_frames, row, col, channel_number), dtype=np.float)
    y = np.zeros((n_samples, n_frames, row, col, 1),
                 dtype=np.float)

    file_list = np.genfromtxt(stage + '_dataset.txt', dtype='str')
    # file_list = file_list[::-1]

    if stage == 'test':
        label = np.zeros(n_samples, dtype=object)
    else:
        shuffle(file_list)  # If we are training, we want to Shuffle order of file list but if testing, we want to keep
        # the same order for better comparision

    current_dir = os.getcwd()

    if channel_number == 1:
        os.chdir('/home/gcx/lstm_sequences/entire_dataset/' + stage + '/predictions_' + stage + '/')
    else:
        os.chdir('/home/gcx/lstm_sequences/entire_dataset/' + stage + '/multi_channel_prediction/')

    if len(file_list)< n_samples:
        sys.exit('not enough sample in the designated set. Exiting script!')

    # Load samples for X
    for i in range(n_samples):

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for j in range(n_frames):
                if channel_number == 1: # If we are considering gray-scale images
                    try:
                        temp_img = cv2.cvtColor(cv2.imread(current_file_list[j]), cv2.COLOR_BGR2GRAY)
                        temp_img /= 255
                        x[i, j , :, :, 0] = temp_img
                    except:
                        sys.exit('could not load image: ')

                elif channel_number > 3:  # If we consider multi-channel images
                    try:
                        temp_img = np.load(current_file_list[j][:-4] + '.npy')
                        temp_img = temp_img / 255.0
                        x[i, j, :, :, :] = temp_img
                    except:
                        sys.exit('could not load npy: ')

                else:
                    sys.exit('incorrect number of channels in X!!')

    # Load data for Y
    os.chdir('/home/gcx/lstm_sequences/entire_dataset/' + stage + '/y_' + stage + '/')
    if len(file_list) < n_samples:
        sys.exit('not enough sample in the designated set. Exiting script!')

    for i in range(n_samples):

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for j in range(n_frames):
                if y.shape[4] == 1:
                    try:
                        temp_img = cv2.cvtColor(cv2.imread(current_file_list[j][:-4] + '.tiff'), cv2.COLOR_BGR2GRAY)
                        temp_img /= 255
                        y[i, j, :, :, 0] = temp_img

                        if stage == 'test' and j == n_frames-1 :
                            file_name = current_file_list[j][:-4] + '.tiff'
                            label[i] = file_name

                    except:
                        sys.exit('could not load image: ')
                else:
                    sys.exit('incorrect number of channels in y!!')

    os.chdir(current_dir)

    if stage == 'test':
        return x, y, label
    else:
        return x, y


# Function to generate a list of files to be processed
def generate_sequences_batch_from_list(start_instant, stop_instant, img_size, map_size,  n_frames, channel_number, stage='test'):

    n_samples = stop_instant - start_instant

    x = np.zeros((n_samples, n_frames, img_size, img_size, channel_number), dtype=np.float)
    y = np.zeros((n_samples, n_frames, map_size, map_size, 1), dtype=np.float)

    # file_list = np.genfromtxt('run_' + stage + '_dataset.txt', dtype='str')
    # file_list = np.genfromtxt('pos_seq/' + stage + '_dataset_' + str(n_frames) + '.txt', dtype='str')
    file_list = np.genfromtxt('neg_n_pos_seq/entire_' + stage + '_dataset_' + str(n_frames) + '.txt', dtype='str')
    # file_list = file_list[::-1]

    if stage == 'test':
        label = np.zeros(n_samples, dtype=object)
    else:
        shuffle(
            file_list)  # If we are training, we want to Shuffle order of file list but if testing, we want to keep
        # the same order for better comparision

    current_dir = os.getcwd()

    if channel_number == 1:
        os.chdir('/home/gcx/lstm_sequences/entire_dataset/' + stage + '/predictions_' + stage + '/')
    elif channel_number == 3:
        # os.chdir('/home/gcx/lstm_sequences/entire_dataset/' + stage + '/x_' + stage + '/')
        os.chdir('/home/gcx/lstm_sequences/entire_dataset/combined/' + stage + '/x_' + stage + '/')
    else:
        os.chdir('/home/gcx/lstm_sequences/entire_dataset/' + stage + '/multi_channel_prediction/')

    if len(file_list) < n_samples:
        sys.exit('not enough samples in the designated set. Exiting script!')

    # Load samples for X
    for i in range(n_samples):

        current_file_list = file_list[i + start_instant, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for j in range(n_frames):
                if channel_number == 1:  # If we are considering gray-scale images
                    try:
                        temp_img = cv2.cvtColor(cv2.imread(current_file_list[j]), cv2.COLOR_BGR2GRAY)
                        temp_img /= 255
                        x[i, j, :, :, 0] = temp_img
                    except:
                        sys.exit('could not load image: ')

                elif channel_number == 3:
                    try:
                        temp_img = cv2.imread(current_file_list[j])
                        temp_img = cv2.resize(temp_img, (img_size, img_size))
                        temp_img = temp_img / 255.

                        x[i, j, :, :, :] = temp_img
                    except:
                        sys.exit('error loading rgb image')

                elif channel_number > 3:  # If we consider multi-channel images
                    try:
                        temp_img = np.load(current_file_list[j][:-4] + '.npy')
                        temp_img = temp_img / 255.0
                        x[i, j, :, :, :] = temp_img
                    except:
                        sys.exit('could not load npy: ')

                else:
                    sys.exit('incorrect number of channels in X!!')

    # Load data for Y
    os.chdir('/home/gcx/lstm_sequences/entire_dataset/combined/' + stage + '/y_' + stage + '/')
    # os.chdir('/home/gcx/lstm_sequences/entire_dataset/' + stage + '/y_' + stage + '/')
    if len(file_list) < n_samples:
        sys.exit('not enough sample in the designated set. Exiting script!')

    for i in range(n_samples):

        current_file_list = file_list[i+start_instant, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for j in range(n_frames):
                if y.shape[4] == 1:
                    try:
                        temp_img = cv2.cvtColor(cv2.imread(current_file_list[j][:-4] + '.tiff'), cv2.COLOR_BGR2GRAY)
                        temp_img /= 255
                        temp_img = cv2.resize(temp_img, (map_size, map_size), cv2.INTER_NEAREST)
                        y[i, j, :, :, 0] = temp_img

                        if stage == 'test' and j == n_frames - 1:
                            file_name = current_file_list[j][:-4] + '.tiff'
                            label[i] = file_name

                    except:
                        sys.exit('could not load gt image: ')
                else:
                    sys.exit('incorrect number of channels in y!!')

    os.chdir(current_dir)

    if stage == 'test':
        return x, y, label, len(file_list)
    else:
        return x, y


# Function to generate a list of files to be processed
def generate_strided_sequences_batch_from_list(start_sequence_indicator,
                                               stop_sequence_indicator,
                                               img_size, map_size,
                                               n_frames, gt_frames,
                                               channel_number=3,
                                               stage='test'):

    n_sequences_to_load = stop_sequence_indicator - start_sequence_indicator

    n_frames_to_load = gt_frames
    stride = int(n_frames / gt_frames)

    x = np.zeros((n_sequences_to_load, n_frames_to_load, img_size, img_size, channel_number), dtype=np.float)
    y = np.zeros((n_sequences_to_load, n_frames_to_load, map_size, map_size, 1),
                 dtype=np.float)

    # file_list = np.genfromtxt('run_' + stage + '_dataset.txt', dtype='str')
    # file_list = np.genfromtxt('pos_seq/' + stage + '_dataset_40.txt', dtype='str')
    file_list = np.genfromtxt('neg_n_pos_seq/entire_' + stage + '_dataset_40.txt', dtype='str')
    # file_list = np.genfromtxt('neg_n_pos_seq/comb_' + stage + '_dataset_40.txt', dtype='str')
    # file_list = file_list[::-1]

    if stage == 'test':
        label = np.zeros(n_sequences_to_load, dtype=object)
    else:
        shuffle(
            file_list)  # If we are training, we want to Shuffle order of file list but if testing, we want to keep
        # the same order for better comparision

    current_dir = os.getcwd()

    if channel_number == 1:
        os.chdir('/home/gcx/lstm_sequences/entire_dataset/' + stage + '/predictions_' + stage + '/')
    elif channel_number == 3:
        os.chdir('/home/gcx/lstm_sequences/entire_dataset/' + stage + '/x_' + stage + '/')
        # os.chdir('/home/gcx/lstm_sequences/entire_dataset/combined/' + stage + '/x_' + stage + '/')
    else:
        os.chdir('/home/gcx/lstm_sequences/entire_dataset/' + stage + '/multi_channel_prediction/')

    if len(file_list) < n_sequences_to_load:
        sys.exit('not enough sequences in the designated set. Exiting script!')

    # Load samples for X
    for i in range(n_sequences_to_load): # i is the pointer of the sequence to load

        current_file_list = file_list[i + start_sequence_indicator, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for time_instant_idx in range(gt_frames):
                if channel_number == 1:  # If we are considering gray-scale images
                    try:
                        temp_img = cv2.cvtColor(cv2.imread(current_file_list[time_instant_idx * stride]), cv2.COLOR_BGR2GRAY)
                        temp_img /= 255
                        x[i, time_instant_idx, :, :, 0] = temp_img
                    except:
                        sys.exit('could not load image: ')

                elif channel_number == 3:
                    # try:

                    temp_img = cv2.imread(current_file_list[time_instant_idx * stride])
                    temp_img = cv2.resize(temp_img, (img_size, img_size))
                    temp_img = temp_img / 255.

                    x[i, time_instant_idx, :, :, :] = temp_img
                    # except:
                    #     sys.exit('error loading rgb image')

                else:
                    sys.exit('incorrect number of channels in X!!')

    # Load data for Y
    # os.chdir('/home/gcx/lstm_sequences/entire_dataset/combined/' + stage + '/y_' + stage + '/')
    os.chdir('/home/gcx/lstm_sequences/entire_dataset/' + stage + '/y_' + stage + '/')
    if len(file_list) < n_sequences_to_load:
        sys.exit('not enough sample in the designated set. Exiting script!')

    for i in range(n_sequences_to_load):

        current_file_list = file_list[i+start_sequence_indicator, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for time_instant_idx in range(gt_frames):
                if y.shape[4] == 1:
                    # try:
                    # print(current_file_list[time_instant_idx * stride][:-4] + '.tiff')
                    temp_img = cv2.cvtColor(
                                            cv2.imread(current_file_list[time_instant_idx * stride][:-4] + '.tiff'),
                                            cv2.COLOR_BGR2GRAY)
                    temp_img /= 255
                    temp_img = cv2.resize(temp_img, (map_size, map_size), cv2.INTER_NEAREST)
                    y[i, time_instant_idx, :, :, 0] = temp_img

                    if stage == 'test' and time_instant_idx == gt_frames - 1:
                        file_name = current_file_list[time_instant_idx * stride][:-4] + '.tiff'
                        label[i] = file_name

                    # except:
                    #     sys.exit('could not load gt image: ')
                else:
                    sys.exit('incorrect number of channels in y!!')

    os.chdir(current_dir)

    if stage == 'test':
        return x, y, label, len(file_list)
    else:
        return x, y


#  function to return the lstm network  for multi-channel data
def network_definition_kernel1(channel_number, full_image_size, reduced_size, n_frames, n_gt_frames):
    # This function was designed to have the vgg layers to extract features and then use convLStm layers with kernel size=(1,1)
    input_img = Input(shape=(n_gt_frames, full_image_size, full_image_size, channel_number))

    model_vgg16_conv = VGG16(weights='imagenet', include_top=False)

    conv1 = TimeDistributed(model_vgg16_conv.layers[1])(input_img)
    conv2 = TimeDistributed(model_vgg16_conv.layers[2])(conv1)
    conv3 = TimeDistributed(model_vgg16_conv.layers[3])(conv2)
    conv4 = TimeDistributed(model_vgg16_conv.layers[4])(conv3)
    conv5 = TimeDistributed(model_vgg16_conv.layers[5])(conv4)
    conv6 = TimeDistributed(model_vgg16_conv.layers[6])(conv5)
    conv7 = TimeDistributed(model_vgg16_conv.layers[7])(conv6)
    conv8 = TimeDistributed(model_vgg16_conv.layers[8])(conv7)
    conv9 = TimeDistributed(model_vgg16_conv.layers[9])(conv8)
    conv9 = TimeDistributed(model_vgg16_conv.layers[10])(conv9)
    conv9 = TimeDistributed(model_vgg16_conv.layers[11])(conv9)
    conv9 = TimeDistributed(model_vgg16_conv.layers[12])(conv9)
    conv9 = TimeDistributed(model_vgg16_conv.layers[13])(conv9)

    x4 = ConvLSTM2D(filters=32, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM3')(conv9)
    x4 = BatchNormalization()(x4)
    x4 = TimeDistributed(MaxPooling2D((2, 2), padding='same'))(x4)
    x5 = ConvLSTM2D(filters=16, kernel_size=(1, 1),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM4')(x4)
    x5 = BatchNormalization()(x5)
    x6 = ConvLSTM2D(filters=8, kernel_size=(1, 1),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM5')(x5)
    x6 = BatchNormalization()(x6)
    # x8 = Conv3D(filters=1, kernel_size=(3, 3, 3),
    #             padding='same', data_format='channels_last')(x6)
    x8 = ConvLSTM2D(filters=1, kernel_size=(1, 1),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM6')(x6)
    # x9 = Conv3D(filters=1,
    #             kernel_size=(3, 1, 1), data_format='channels_last', padding='same',
    #             activation='sigmoid')(x8)

    seq = Model(input_img, x8)

    seq.layers[1].trainable = False
    seq.layers[2].trainable = False
    seq.layers[3].trainable = False
    seq.layers[4].trainable = False
    seq.layers[5].trainable = False
    seq.layers[6].trainable = False
    seq.layers[7].trainable = False
    seq.layers[8].trainable = False
    seq.layers[9].trainable = False
    seq.layers[10].trainable = False
    seq.layers[11].trainable = False
    seq.layers[12].trainable = False
    seq.layers[13].trainable = False
    seq.layers[14].trainable = False

    return seq


#  function to return the lstm network  for multi-channel data
def network_definition_mixed(channel_number, full_image_size, reduced_size, n_frames, n_gt_frames):
    input_img = Input(shape=(n_gt_frames, full_image_size, full_image_size, channel_number))

    model_vgg16_conv = VGG16(weights='imagenet', include_top=False)

    conv1 = TimeDistributed(model_vgg16_conv.layers[1])(input_img)
    conv2 = TimeDistributed(model_vgg16_conv.layers[2])(conv1)
    # conv3 = TimeDistributed(model_vgg16_conv.layers[3])(conv2)
    conv3 = TimeDistributed(AveragePooling2D((2,2)))(conv2)
    conv4 = TimeDistributed(model_vgg16_conv.layers[4])(conv3)
    conv5 = TimeDistributed(model_vgg16_conv.layers[5])(conv4)
    # conv6 = TimeDistributed(model_vgg16_conv.layers[6])(conv5)
    conv6 = TimeDistributed(AveragePooling2D((2,2)))(conv5)
    conv7 = TimeDistributed(model_vgg16_conv.layers[7])(conv6)
    conv8 = TimeDistributed(model_vgg16_conv.layers[8])(conv7)
    conv9 = TimeDistributed(model_vgg16_conv.layers[9])(conv8)
    # conv9 = TimeDistributed(model_vgg16_conv.layers[10])(conv9)
    conv9 = TimeDistributed(AveragePooling2D((2,2)))(conv9)
    conv9 = TimeDistributed(model_vgg16_conv.layers[11])(conv9)
    conv9 = TimeDistributed(model_vgg16_conv.layers[12])(conv9)
    conv9 = TimeDistributed(model_vgg16_conv.layers[13])(conv9)

    # x3 = ConvLSTM2D(filters=32, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='cLSTM3')(conv9)
    # x3 = BatchNormalization()(x3)

    # i have been using 'sama' padding but will try to use valid padding
    # x3 = TimeDistributed(MaxPooling2D((2, 2), padding='same'))(conv9)
    x3 = TimeDistributed(AveragePooling2D((2, 2), padding='same'))(conv9)

    x4 = ConvLSTM2D(filters=32, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM3')(x3)
    x4 = BatchNormalization()(x4)
    # x4 = TimeDistributed(MaxPooling2D((2, 2), padding='same'))(x4)
    x5 = ConvLSTM2D(filters=16, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM4')(x4)
    x5 = BatchNormalization()(x5)
    x6 = ConvLSTM2D(filters=8, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM5')(x5)
    x6 = BatchNormalization()(x6)
    # x8 = Conv3D(filters=1, kernel_size=(3, 3, 3),
    #             padding='same', data_format='channels_last')(x6)
    x8 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='sigmoid', name='cLSTM6')(x6)

    seq = Model(input_img, x8)

    seq.layers[1].trainable = False
    seq.layers[2].trainable = False
    seq.layers[3].trainable = False
    seq.layers[4].trainable = False
    seq.layers[5].trainable = False
    seq.layers[6].trainable = False
    seq.layers[7].trainable = False
    seq.layers[8].trainable = False
    seq.layers[9].trainable = False
    seq.layers[10].trainable = False
    seq.layers[11].trainable = False
    seq.layers[12].trainable = False
    seq.layers[13].trainable = False
    # seq.layers[14].trainable = False

    return seq


#  function to return the lstm network  for multi-channel data
def network_definition_mixed_multiscale(channel_number, full_image_size, reduced_size, n_frames, n_gt_frames):
    input_img = Input(shape=(n_gt_frames, full_image_size, full_image_size, channel_number))

    model_vgg16_conv = VGG16(weights='imagenet', include_top=False)

    conv1 = TimeDistributed(model_vgg16_conv.layers[1])(input_img)
    conv2 = TimeDistributed(model_vgg16_conv.layers[2])(conv1)
    conv3 = TimeDistributed(model_vgg16_conv.layers[3])(conv2)
    conv4 = TimeDistributed(model_vgg16_conv.layers[4])(conv3)
    conv5 = TimeDistributed(model_vgg16_conv.layers[5])(conv4)
    conv6 = TimeDistributed(model_vgg16_conv.layers[6])(conv5)
    conv7 = TimeDistributed(model_vgg16_conv.layers[7])(conv6)
    conv8 = TimeDistributed(model_vgg16_conv.layers[8])(conv7)
    conv9 = TimeDistributed(model_vgg16_conv.layers[9])(conv8)
    conv10 = TimeDistributed(model_vgg16_conv.layers[10])(conv9)  # pooling
    conv11 = TimeDistributed(model_vgg16_conv.layers[11])(conv10)
    # conv111 = TimeDistributed(Dropout(0.5))(conv11)
    conv12 = TimeDistributed(model_vgg16_conv.layers[12])(conv11)
    conv13 = TimeDistributed(model_vgg16_conv.layers[13])(conv12)

    zz = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
                                padding='same', activation='relu',
                                name='conv1_zz'))(conv5)


    z2 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3),
                                padding='same', activation='relu',
                                name='conv1_z'))(conv9)
    z3 = BatchNormalization()(z2)
    z31 = TimeDistributed(Dropout(0.5))(z3)
    z4 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
                    padding='same', activation='relu', name='conv2_z'))(z31)
    z4 = BatchNormalization()(z4)
    z41 = TimeDistributed(Dropout(0.5))(z4)
    # z5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='cLSTM_z')(z4)
    z5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
                                padding='same', activation='relu'))(z41)
    z6 = BatchNormalization()(z5)
    z7 = TimeDistributed(UpSampling2D(size=(2, 2)))(z6)
    # z62 = TimeDistributed(Dropout(0.5))(z6)

    y1 = TimeDistributed(Conv2D(filters=32, kernel_size=(3,3), padding='same', activation='relu', name='conv1_y'))(conv13)
    y1 = BatchNormalization()(y1)
    y11 = TimeDistributed(Dropout(0.5))(y1)
    y2 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
                    padding='same', activation='relu', name='clstm_y1'))(y11)
    y2 = BatchNormalization()(y2)
    y21 = TimeDistributed(Dropout(0.5))(y2)
    # y3 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='clstm_y2')(y2)
    y3 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
                                padding='same', activation='relu'))(y21)
    y4 = BatchNormalization()(y3)
    # y41 = TimeDistributed(Dropout(0.5))(y4)
    y5 = TimeDistributed(UpSampling2D(size=(4, 4)))(y4)

    # x1 = TimeDistributed(MaxPooling2D((2, 2), padding='same'))(conv9)
    x1 = TimeDistributed(AveragePooling2D((2, 2), padding='same'))(conv13)
    x2 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='relu', name='conv1_x'))(x1)
    x3 = BatchNormalization()(x2)
    x31 = TimeDistributed(Dropout(0.5))(x3)
    x4 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
                    padding='same', activation='relu', name='cLSTM_x1'))(x31)
    x4 = BatchNormalization()(x4)
    x41 = TimeDistributed(Dropout(0.5))(x4)
    # x5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='cLSTM_x2')(x4)
    x5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
                                padding='same', activation='relu'))(x41)
    x6 = BatchNormalization()(x5)
    # x61 = TimeDistributed(Dropout(0.5))(x6)

    # x8 = Conv3D(filters=1, kernel_size=(3, 3, 3),
    #             padding='same', data_format='channels_last')(x6)
    # x7 = ConvLSTM2D(filters=1, kernel_size=(1, 1),
    #                 padding='same', return_sequences=True,
    #                 activation='sigmoid', name='cLSTM_x3')(x6)
    x7 = TimeDistributed(UpSampling2D(size=(2, 2)))(x6)
    # x9 = TimeDistributed(Lambda(lambda x: x[: 5625]))(x8)
    x8 = Lambda(lambda x: x[:, :, :75, :75, :])(x7)
    x9 = TimeDistributed(UpSampling2D(size=(4, 4)))(x8)

    w1 = Concatenate(axis=4)([x9, y5, z7, zz])

    w2 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='sigmoid', name='cLSTM_w1')(w1)

    seq = Model(input_img, w2)

    seq.layers[1].trainable = False
    seq.layers[2].trainable = False
    seq.layers[3].trainable = False
    seq.layers[4].trainable = False
    seq.layers[5].trainable = False
    seq.layers[6].trainable = False
    seq.layers[7].trainable = False
    seq.layers[8].trainable = False
    seq.layers[9].trainable = False
    seq.layers[10].trainable = False
    seq.layers[11].trainable = False
    seq.layers[12].trainable = False
    seq.layers[13].trainable = False
    # seq.layers[14].trainable = False
    # seq.layers[15].trainable = False
    # seq.layers[16].trainable = False

    # for i in range(1, 16):
    #     print(seq.layers[i].layer.name)

    return seq


#  function to return the lstm network  for multi-channel data
def network_mixed_multiclear(channel_number, full_image_size, reduced_size, n_frames, n_gt_frames):
    input_img = Input(shape=(n_gt_frames, full_image_size, full_image_size, channel_number))

    model_vgg16_conv = VGG16(weights='imagenet', include_top=False)

    conv1 = TimeDistributed(model_vgg16_conv.layers[1])(input_img)
    conv2 = TimeDistributed(model_vgg16_conv.layers[2])(conv1)
    conv3 = TimeDistributed(model_vgg16_conv.layers[3])(conv2)
    conv4 = TimeDistributed(model_vgg16_conv.layers[4])(conv3)
    conv5 = TimeDistributed(model_vgg16_conv.layers[5])(conv4)
    conv6 = TimeDistributed(model_vgg16_conv.layers[6])(conv5)
    conv7 = TimeDistributed(model_vgg16_conv.layers[7])(conv6)
    conv8 = TimeDistributed(model_vgg16_conv.layers[8])(conv7)
    conv9 = TimeDistributed(model_vgg16_conv.layers[9])(conv8)
    conv10 = TimeDistributed(model_vgg16_conv.layers[10])(conv9)  # pooling
    conv11 = TimeDistributed(model_vgg16_conv.layers[11])(conv10)
    # conv111 = TimeDistributed(Dropout(0.5))(conv11)
    conv12 = TimeDistributed(model_vgg16_conv.layers[12])(conv11)
    conv13 = TimeDistributed(model_vgg16_conv.layers[13])(conv12)
    conv14 = TimeDistributed(model_vgg16_conv.layers[14])(conv13)
    conv15 = TimeDistributed(model_vgg16_conv.layers[15])(conv14)
    conv16 = TimeDistributed(model_vgg16_conv.layers[16])(conv15)
    conv17 = TimeDistributed(model_vgg16_conv.layers[17])(conv16)

    zz = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
                                padding='same', activation='relu',
                                name='conv1_zz'))(conv5)

    z5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
                                padding='same', activation='relu',
                                name='conv1_z'))(conv9)
    # z3 = BatchNormalization()(z2)
    # z31 = TimeDistributed(Dropout(0.5))(z3)
    # z4 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='conv2_z'))(z31)
    # z4 = BatchNormalization()(z4)
    # z41 = TimeDistributed(Dropout(0.5))(z4)
    #
    # # z5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    # #                 padding='same', return_sequences=True,
    # #                 activation='relu', name='cLSTM_z')(z4)
    # z5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
    #                             padding='same', activation='relu'))(z41)
    z6 = BatchNormalization()(z5)
    z7 = TimeDistributed(UpSampling2D(size=(2, 2)))(z6)
    # z62 = TimeDistributed(Dropout(0.5))(z6)

    y1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3), padding='same', activation='relu', name='conv1_y'))(conv13)
    y4 = BatchNormalization()(y1)
    # y11 = TimeDistributed(Dropout(0.5))(y1)
    # y2 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='clstm_y1'))(y11)
    # y2 = BatchNormalization()(y2)
    # y21 = TimeDistributed(Dropout(0.5))(y2)
    # # y3 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    # #                 padding='same', return_sequences=True,
    # #                 activation='relu', name='clstm_y2')(y2)
    # y3 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
    #                             padding='same', activation='relu'))(y21)
    # y4 = BatchNormalization()(y3)
    # y41 = TimeDistributed(Dropout(0.5))(y4)
    y5 = TimeDistributed(UpSampling2D(size=(4, 4)))(y4)

    # x1 = TimeDistributed(MaxPooling2D((2, 2), padding='same'))(conv9)
    # x1 = TimeDistributed(AveragePooling2D((2, 2), padding='same'))(conv13)
    x1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3), padding='same', activation='relu', name='conv1_x'))(conv17)
    x2 = BatchNormalization()(x1)
    # x31 = TimeDistributed(Dropout(0.5))(x3)
    # x4 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='cLSTM_x1'))(x31)
    # x4 = BatchNormalization()(x4)
    # x41 = TimeDistributed(Dropout(0.5))(x4)
    # x5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='cLSTM_x2')(x4)
    # x5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
    #                             padding='same', activation='relu'))(x41)
    # x6 = BatchNormalization()(x5)
    # x61 = TimeDistributed(Dropout(0.5))(x6)

    # x8 = Conv3D(filters=1, kernel_size=(3, 3, 3),
    #             padding='same', data_format='channels_last')(x6)
    # x7 = ConvLSTM2D(filters=1, kernel_size=(1, 1),
    #                 padding='same', return_sequences=True,
    #                 activation='sigmoid', name='cLSTM_x3')(x6)
    x7 = TimeDistributed(UpSampling2D(size=(2, 2)))(x2)
    # x9 = TimeDistributed(Lambda(lambda x: x[: 5625]))(x8)
    x8 = Lambda(lambda x: x[:, :, :75, :75, :])(x7)
    x9 = TimeDistributed(UpSampling2D(size=(4, 4)))(x8)
    x10 = TimeDistributed(ZeroPadding2D((2,2)))(x9)

    w1 = Concatenate(axis=4)([x10, y5, z7, zz])

    w2 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='sigmoid', name='cLSTM_w1')(w1)

    seq = Model(input_img, w2)

    seq.layers[1].trainable = False
    seq.layers[2].trainable = False
    seq.layers[3].trainable = False
    seq.layers[4].trainable = False
    seq.layers[5].trainable = False
    seq.layers[6].trainable = False
    seq.layers[7].trainable = False
    seq.layers[8].trainable = False
    seq.layers[9].trainable = False
    seq.layers[10].trainable = False
    seq.layers[11].trainable = False
    seq.layers[12].trainable = False
    seq.layers[13].trainable = False
    # seq.layers[14].trainable = False
    # seq.layers[15].trainable = False
    # seq.layers[16].trainable = False

    # for i in range(1, 16):
    #     print(seq.layers[i].layer.name)

    return seq


def network_conv2d(channel_number, full_image_size, reduced_size, n_frames, n_gt_frames):
    input_img = Input(shape=(n_gt_frames, full_image_size, full_image_size, channel_number))

    model_vgg16_conv = VGG16(weights='imagenet', include_top=False)

    conv1 = TimeDistributed(model_vgg16_conv.layers[1])(input_img)
    conv2 = TimeDistributed(model_vgg16_conv.layers[2])(conv1)
    conv3 = TimeDistributed(model_vgg16_conv.layers[3])(conv2)
    conv4 = TimeDistributed(model_vgg16_conv.layers[4])(conv3)
    conv5 = TimeDistributed(model_vgg16_conv.layers[5])(conv4)
    conv6 = TimeDistributed(model_vgg16_conv.layers[6])(conv5)
    conv7 = TimeDistributed(model_vgg16_conv.layers[7])(conv6)
    conv8 = TimeDistributed(model_vgg16_conv.layers[8])(conv7)
    conv9 = TimeDistributed(model_vgg16_conv.layers[9])(conv8)
    conv10 = TimeDistributed(model_vgg16_conv.layers[10])(conv9)  # pooling
    conv11 = TimeDistributed(model_vgg16_conv.layers[11])(conv10)
    # conv111 = TimeDistributed(Dropout(0.5))(conv11)
    conv12 = TimeDistributed(model_vgg16_conv.layers[12])(conv11)
    conv13 = TimeDistributed(model_vgg16_conv.layers[13])(conv12)
    conv14 = TimeDistributed(model_vgg16_conv.layers[14])(conv13)
    conv15 = TimeDistributed(model_vgg16_conv.layers[15])(conv14)
    conv16 = TimeDistributed(model_vgg16_conv.layers[16])(conv15)
    conv17 = TimeDistributed(model_vgg16_conv.layers[17])(conv16)

    zz = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
                                padding='same', activation='relu',
                                name='conv1_zz'))(conv5)

    z5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
                                padding='same', activation='relu',
                                name='conv1_z'))(conv9)
    # z3 = BatchNormalization()(z2)
    # z31 = TimeDistributed(Dropout(0.5))(z3)
    # z4 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='conv2_z'))(z31)
    # z4 = BatchNormalization()(z4)
    # z41 = TimeDistributed(Dropout(0.5))(z4)
    #
    # # z5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    # #                 padding='same', return_sequences=True,
    # #                 activation='relu', name='cLSTM_z')(z4)
    # z5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
    #                             padding='same', activation='relu'))(z41)
    z6 = BatchNormalization()(z5)
    z7 = TimeDistributed(UpSampling2D(size=(2, 2)))(z6)
    # z62 = TimeDistributed(Dropout(0.5))(z6)

    y1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3), padding='same', activation='relu', name='conv1_y'))(conv13)
    y4 = BatchNormalization()(y1)
    # y11 = TimeDistributed(Dropout(0.5))(y1)
    # y2 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='clstm_y1'))(y11)
    # y2 = BatchNormalization()(y2)
    # y21 = TimeDistributed(Dropout(0.5))(y2)
    # # y3 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    # #                 padding='same', return_sequences=True,
    # #                 activation='relu', name='clstm_y2')(y2)
    # y3 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
    #                             padding='same', activation='relu'))(y21)
    # y4 = BatchNormalization()(y3)
    # y41 = TimeDistributed(Dropout(0.5))(y4)
    y5 = TimeDistributed(UpSampling2D(size=(4, 4)))(y4)

    # x1 = TimeDistributed(MaxPooling2D((2, 2), padding='same'))(conv9)
    # x1 = TimeDistributed(AveragePooling2D((2, 2), padding='same'))(conv13)
    x1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3), padding='same', activation='relu', name='conv1_x'))(conv17)
    x2 = BatchNormalization()(x1)
    # x31 = TimeDistributed(Dropout(0.5))(x3)
    # x4 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='cLSTM_x1'))(x31)
    # x4 = BatchNormalization()(x4)
    # x41 = TimeDistributed(Dropout(0.5))(x4)
    # x5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='cLSTM_x2')(x4)
    # x5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
    #                             padding='same', activation='relu'))(x41)
    # x6 = BatchNormalization()(x5)
    # x61 = TimeDistributed(Dropout(0.5))(x6)

    # x8 = Conv3D(filters=1, kernel_size=(3, 3, 3),
    #             padding='same', data_format='channels_last')(x6)
    # x7 = ConvLSTM2D(filters=1, kernel_size=(1, 1),
    #                 padding='same', return_sequences=True,
    #                 activation='sigmoid', name='cLSTM_x3')(x6)
    x7 = TimeDistributed(UpSampling2D(size=(2, 2)))(x2)
    # x9 = TimeDistributed(Lambda(lambda x: x[: 5625]))(x8)
    x8 = Lambda(lambda x: x[:, :, :75, :75, :])(x7)
    x9 = TimeDistributed(UpSampling2D(size=(4, 4)))(x8)
    x10 = TimeDistributed(ZeroPadding2D((2,2)))(x9)

    w1 = Concatenate(axis=4)([x10, y5, z7, zz])

    w2 = TimeDistributed(Conv2D(filters=1,
                                kernel_size=(3,3),
                                padding='same',
                                activation='sigmoid',
                                name='Conv2d_w1'))(w1)

    seq = Model(input_img, w2)

    seq.layers[1].trainable = False
    seq.layers[2].trainable = False
    seq.layers[3].trainable = False
    seq.layers[4].trainable = False
    seq.layers[5].trainable = False
    seq.layers[6].trainable = False
    seq.layers[7].trainable = False
    seq.layers[8].trainable = False
    seq.layers[9].trainable = False
    seq.layers[10].trainable = False
    seq.layers[11].trainable = False
    seq.layers[12].trainable = False
    seq.layers[13].trainable = False
    # seq.layers[14].trainable = False
    # seq.layers[15].trainable = False
    # seq.layers[16].trainable = False

    # for i in range(1, 16):
    #     print(seq.layers[i].layer.name)

    return seq


def network_lstm(channel_number, full_image_size, reduced_size, n_frames, n_gt_frames):
    input_img = Input(shape=(n_gt_frames, full_image_size, full_image_size, channel_number))

    model_vgg16_conv = VGG16(weights='imagenet', include_top=False)

    conv1 = TimeDistributed(model_vgg16_conv.layers[1])(input_img)
    conv2 = TimeDistributed(model_vgg16_conv.layers[2])(conv1)
    conv3 = TimeDistributed(model_vgg16_conv.layers[3])(conv2)
    conv4 = TimeDistributed(model_vgg16_conv.layers[4])(conv3)
    conv5 = TimeDistributed(model_vgg16_conv.layers[5])(conv4)
    conv6 = TimeDistributed(model_vgg16_conv.layers[6])(conv5)
    conv7 = TimeDistributed(model_vgg16_conv.layers[7])(conv6)
    conv8 = TimeDistributed(model_vgg16_conv.layers[8])(conv7)
    conv9 = TimeDistributed(model_vgg16_conv.layers[9])(conv8)
    conv10 = TimeDistributed(model_vgg16_conv.layers[10])(conv9)  # pooling
    conv11 = TimeDistributed(model_vgg16_conv.layers[11])(conv10)
    # conv111 = TimeDistributed(Dropout(0.5))(conv11)
    conv12 = TimeDistributed(model_vgg16_conv.layers[12])(conv11)
    conv13 = TimeDistributed(model_vgg16_conv.layers[13])(conv12)
    conv14 = TimeDistributed(model_vgg16_conv.layers[14])(conv13)
    conv15 = TimeDistributed(model_vgg16_conv.layers[15])(conv14)
    conv16 = TimeDistributed(model_vgg16_conv.layers[16])(conv15)
    conv17 = TimeDistributed(model_vgg16_conv.layers[17])(conv16)

    zz = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
                                padding='same', activation='relu',
                                name='conv1_zz'))(conv5)

    z5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
                                padding='same', activation='relu',
                                name='conv1_z'))(conv9)
    # z3 = BatchNormalization()(z2)
    # z31 = TimeDistributed(Dropout(0.5))(z3)
    # z4 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='conv2_z'))(z31)
    # z4 = BatchNormalization()(z4)
    # z41 = TimeDistributed(Dropout(0.5))(z4)
    #
    # # z5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    # #                 padding='same', return_sequences=True,
    # #                 activation='relu', name='cLSTM_z')(z4)
    # z5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
    #                             padding='same', activation='relu'))(z41)
    z6 = BatchNormalization()(z5)
    z7 = TimeDistributed(UpSampling2D(size=(2, 2)))(z6)
    # z62 = TimeDistributed(Dropout(0.5))(z6)

    y1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3), padding='same', activation='relu', name='conv1_y'))(conv13)
    y4 = BatchNormalization()(y1)
    # y11 = TimeDistributed(Dropout(0.5))(y1)
    # y2 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='clstm_y1'))(y11)
    # y2 = BatchNormalization()(y2)
    # y21 = TimeDistributed(Dropout(0.5))(y2)
    # # y3 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    # #                 padding='same', return_sequences=True,
    # #                 activation='relu', name='clstm_y2')(y2)
    # y3 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
    #                             padding='same', activation='relu'))(y21)
    # y4 = BatchNormalization()(y3)
    # y41 = TimeDistributed(Dropout(0.5))(y4)
    y5 = TimeDistributed(UpSampling2D(size=(4, 4)))(y4)

    # x1 = TimeDistributed(MaxPooling2D((2, 2), padding='same'))(conv9)
    # x1 = TimeDistributed(AveragePooling2D((2, 2), padding='same'))(conv13)
    x1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3), padding='same', activation='relu', name='conv1_x'))(conv17)
    x2 = BatchNormalization()(x1)
    # x31 = TimeDistributed(Dropout(0.5))(x3)
    # x4 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='cLSTM_x1'))(x31)
    # x4 = BatchNormalization()(x4)
    # x41 = TimeDistributed(Dropout(0.5))(x4)
    # x5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='cLSTM_x2')(x4)
    # x5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
    #                             padding='same', activation='relu'))(x41)
    # x6 = BatchNormalization()(x5)
    # x61 = TimeDistributed(Dropout(0.5))(x6)

    # x8 = Conv3D(filters=1, kernel_size=(3, 3, 3),
    #             padding='same', data_format='channels_last')(x6)
    # x7 = ConvLSTM2D(filters=1, kernel_size=(1, 1),
    #                 padding='same', return_sequences=True,
    #                 activation='sigmoid', name='cLSTM_x3')(x6)
    x7 = TimeDistributed(UpSampling2D(size=(2, 2)))(x2)
    # x9 = TimeDistributed(Lambda(lambda x: x[: 5625]))(x8)
    x8 = Lambda(lambda x: x[:, :, :75, :75, :])(x7)
    x9 = TimeDistributed(UpSampling2D(size=(4, 4)))(x8)
    x10 = TimeDistributed(ZeroPadding2D((2,2)))(x9)

    w1 = Concatenate(axis=4)([x10, y5, z7, zz])
    w2 = TimeDistributed(MaxPooling2D((4, 4), padding='same'))(w1)
    w3 = TimeDistributed(Flatten())(w2)

    w4 = LSTM(30*30*1, return_sequences=True, activation='sigmoid', name='LSTM_w1')(w3)

    seq = Model(input_img, w4)

    seq.layers[1].trainable = False
    seq.layers[2].trainable = False
    seq.layers[3].trainable = False
    seq.layers[4].trainable = False
    seq.layers[5].trainable = False
    seq.layers[6].trainable = False
    seq.layers[7].trainable = False
    seq.layers[8].trainable = False
    seq.layers[9].trainable = False
    seq.layers[10].trainable = False
    seq.layers[11].trainable = False
    seq.layers[12].trainable = False
    seq.layers[13].trainable = False
    # seq.layers[14].trainable = False
    # seq.layers[15].trainable = False
    # seq.layers[16].trainable = False

    # for i in range(1, 16):
    #     print(seq.layers[i].layer.name)

    return seq


def network_lstm_rescaled(channel_number, full_image_size, reduced_size, n_frames, n_gt_frames):
    input_img = Input(shape=(n_gt_frames, full_image_size, full_image_size, channel_number))

    model_vgg16_conv = VGG16(weights='imagenet', include_top=False)

    conv1 = TimeDistributed(model_vgg16_conv.layers[1])(input_img)
    conv2 = TimeDistributed(model_vgg16_conv.layers[2])(conv1)
    conv3 = TimeDistributed(model_vgg16_conv.layers[3])(conv2)
    conv4 = TimeDistributed(model_vgg16_conv.layers[4])(conv3)
    conv5 = TimeDistributed(model_vgg16_conv.layers[5])(conv4)
    conv6 = TimeDistributed(model_vgg16_conv.layers[6])(conv5)
    conv7 = TimeDistributed(model_vgg16_conv.layers[7])(conv6)
    conv8 = TimeDistributed(model_vgg16_conv.layers[8])(conv7)
    conv9 = TimeDistributed(model_vgg16_conv.layers[9])(conv8)
    conv10 = TimeDistributed(model_vgg16_conv.layers[10])(conv9)  # pooling
    conv11 = TimeDistributed(model_vgg16_conv.layers[11])(conv10)
    # conv111 = TimeDistributed(Dropout(0.5))(conv11)
    conv12 = TimeDistributed(model_vgg16_conv.layers[12])(conv11)
    conv13 = TimeDistributed(model_vgg16_conv.layers[13])(conv12)
    conv14 = TimeDistributed(model_vgg16_conv.layers[14])(conv13)
    conv15 = TimeDistributed(model_vgg16_conv.layers[15])(conv14)
    conv16 = TimeDistributed(model_vgg16_conv.layers[16])(conv15)
    conv17 = TimeDistributed(model_vgg16_conv.layers[17])(conv16)

    zz = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
                                padding='same', activation='relu',
                                name='conv1_zz'))(conv5)

    z5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
                                padding='same', activation='relu',
                                name='conv1_z'))(conv9)
    # z3 = BatchNormalization()(z2)
    # z31 = TimeDistributed(Dropout(0.5))(z3)
    # z4 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='conv2_z'))(z31)
    # z4 = BatchNormalization()(z4)
    # z41 = TimeDistributed(Dropout(0.5))(z4)
    #
    # # z5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    # #                 padding='same', return_sequences=True,
    # #                 activation='relu', name='cLSTM_z')(z4)
    # z5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
    #                             padding='same', activation='relu'))(z41)
    z6 = BatchNormalization()(z5)
    z7 = TimeDistributed(UpSampling2D(size=(2, 2)))(z6)
    # z62 = TimeDistributed(Dropout(0.5))(z6)

    y1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3), padding='same', activation='relu', name='conv1_y'))(conv13)
    y4 = BatchNormalization()(y1)
    # y11 = TimeDistributed(Dropout(0.5))(y1)
    # y2 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='clstm_y1'))(y11)
    # y2 = BatchNormalization()(y2)
    # y21 = TimeDistributed(Dropout(0.5))(y2)
    # # y3 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    # #                 padding='same', return_sequences=True,
    # #                 activation='relu', name='clstm_y2')(y2)
    # y3 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
    #                             padding='same', activation='relu'))(y21)
    # y4 = BatchNormalization()(y3)
    # y41 = TimeDistributed(Dropout(0.5))(y4)
    y5 = TimeDistributed(UpSampling2D(size=(4, 4)))(y4)

    # x1 = TimeDistributed(MaxPooling2D((2, 2), padding='same'))(conv9)
    # x1 = TimeDistributed(AveragePooling2D((2, 2), padding='same'))(conv13)
    x1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3), padding='same', activation='relu', name='conv1_x'))(conv17)
    x2 = BatchNormalization()(x1)
    # x31 = TimeDistributed(Dropout(0.5))(x3)
    # x4 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='cLSTM_x1'))(x31)
    # x4 = BatchNormalization()(x4)
    # x41 = TimeDistributed(Dropout(0.5))(x4)
    # x5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='cLSTM_x2')(x4)
    # x5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
    #                             padding='same', activation='relu'))(x41)
    # x6 = BatchNormalization()(x5)
    # x61 = TimeDistributed(Dropout(0.5))(x6)

    # x8 = Conv3D(filters=1, kernel_size=(3, 3, 3),
    #             padding='same', data_format='channels_last')(x6)
    # x7 = ConvLSTM2D(filters=1, kernel_size=(1, 1),
    #                 padding='same', return_sequences=True,
    #                 activation='sigmoid', name='cLSTM_x3')(x6)
    x7 = TimeDistributed(UpSampling2D(size=(2, 2)))(x2)
    # x9 = TimeDistributed(Lambda(lambda x: x[: 5625]))(x8)
    x8 = Lambda(lambda x: x[:, :, :75, :75, :])(x7)
    x9 = TimeDistributed(UpSampling2D(size=(4, 4)))(x8)
    x10 = TimeDistributed(ZeroPadding2D((2,2)))(x9)

    w1 = Concatenate(axis=4)([x10, y5, z7, zz])
    w2 = TimeDistributed(MaxPooling2D((4, 4), padding='same'))(w1)
    w3 = TimeDistributed(Flatten())(w2)

    w4 = LSTM(30*30*1, return_sequences=True, activation='sigmoid', name='LSTM_w1', unroll=False)(w3)

    w5 = Reshape((n_gt_frames, 30, 30, 1))(w4)
    w6 = TimeDistributed(UpSampling2D(size=(10,10)))(w5)
    w7 = Reshape((n_gt_frames, 300, 300))(w6)
    seq = Model(input_img, w7)


    return seq


#  function to return the lstm network  for multi-channel data
def network_mixed_multiclear_lstm(channel_number, full_image_size, reduced_size, n_frames, n_gt_frames):
    input_img = Input(shape=(n_gt_frames, full_image_size, full_image_size, channel_number))

    model_vgg16_conv = VGG16(weights='imagenet', include_top=False)

    conv1 = TimeDistributed(model_vgg16_conv.layers[1])(input_img)
    conv2 = TimeDistributed(model_vgg16_conv.layers[2])(conv1)
    conv3 = TimeDistributed(model_vgg16_conv.layers[3])(conv2)
    conv4 = TimeDistributed(model_vgg16_conv.layers[4])(conv3)
    conv5 = TimeDistributed(model_vgg16_conv.layers[5])(conv4)
    conv6 = TimeDistributed(model_vgg16_conv.layers[6])(conv5)
    conv7 = TimeDistributed(model_vgg16_conv.layers[7])(conv6)
    conv8 = TimeDistributed(model_vgg16_conv.layers[8])(conv7)
    conv9 = TimeDistributed(model_vgg16_conv.layers[9])(conv8)
    conv10 = TimeDistributed(model_vgg16_conv.layers[10])(conv9)  # pooling
    conv11 = TimeDistributed(model_vgg16_conv.layers[11])(conv10)
    # conv111 = TimeDistributed(Dropout(0.5))(conv11)
    conv12 = TimeDistributed(model_vgg16_conv.layers[12])(conv11)
    conv13 = TimeDistributed(model_vgg16_conv.layers[13])(conv12)
    conv14 = TimeDistributed(model_vgg16_conv.layers[14])(conv13)
    conv15 = TimeDistributed(model_vgg16_conv.layers[15])(conv14)
    conv16 = TimeDistributed(model_vgg16_conv.layers[16])(conv15)
    conv17 = TimeDistributed(model_vgg16_conv.layers[17])(conv16)

    # zz = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
    #                             padding='same', activation='relu',
    #                             name='conv1_zz'))(conv5)
    # zz = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='cLSTM_zz1')(conv5)
    zz = conv5

    # z5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
    #                             padding='same', activation='relu',
    #                             name='conv1_z'))(conv9)
    # z5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='cLSTM_z1')(conv9)
    z5 = conv9

    # z3 = BatchNormalization()(z2)
    # z31 = TimeDistributed(Dropout(0.5))(z3)
    # z4 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='conv2_z'))(z31)
    # z4 = BatchNormalization()(z4)
    # z41 = TimeDistributed(Dropout(0.5))(z4)
    #
    # # z5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    # #                 padding='same', return_sequences=True,
    # #                 activation='relu', name='cLSTM_z')(z4)
    # z5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
    #                             padding='same', activation='relu'))(z41)
    # z6 = BatchNormalization()(z5)
    z6 = (z5)
    z7 = TimeDistributed(UpSampling2D(size=(2, 2)))(z6)
    # z62 = TimeDistributed(Dropout(0.5))(z6)

    # y1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3), padding='same', activation='relu', name='conv1_y'))(conv13)
    # y1 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='cLSTM_x1')(conv13)
    # y4 = BatchNormalization()(y1)
    y4 = conv13
    # y11 = TimeDistributed(Dropout(0.5))(y1)
    # y2 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='clstm_y1'))(y11)
    # y2 = BatchNormalization()(y2)
    # y21 = TimeDistributed(Dropout(0.5))(y2)
    # # y3 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    # #                 padding='same', return_sequences=True,
    # #                 activation='relu', name='clstm_y2')(y2)
    # y3 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
    #                             padding='same', activation='relu'))(y21)
    # y4 = BatchNormalization()(y3)
    # y41 = TimeDistributed(Dropout(0.5))(y4)
    y5 = TimeDistributed(UpSampling2D(size=(4, 4)))(y4)

    # x1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3), padding='same', activation='relu', name='conv1_x'))(conv17)
    # x1 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #            padding='same', return_sequences=True,
    #            activation='relu', name='cLSTM_wx1')(conv17)
    # x2 = BatchNormalization()(x1)
    x2 = conv17
    # x31 = TimeDistributed(Dropout(0.5))(x3)
    # x4 = TimeDistributed(Conv2D(filters=16, kernel_size=(3, 3),
    #                 padding='same', activation='relu', name='cLSTM_x1'))(x31)
    # x4 = BatchNormalization()(x4)
    # x41 = TimeDistributed(Dropout(0.5))(x4)
    # x5 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='cLSTM_x2')(x4)
    # x5 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
    #                             padding='same', activation='relu'))(x41)
    # x6 = BatchNormalization()(x5)
    # x61 = TimeDistributed(Dropout(0.5))(x6)

    # x8 = Conv3D(filters=1, kernel_size=(3, 3, 3),
    #             padding='same', data_format='channels_last')(x6)
    # x7 = ConvLSTM2D(filters=1, kernel_size=(1, 1),
    #                 padding='same', return_sequences=True,
    #                 activation='sigmoid', name='cLSTM_x3')(x6)
    x7 = TimeDistributed(UpSampling2D(size=(2, 2)))(x2)
    # x9 = TimeDistributed(Lambda(lambda x: x[: 5625]))(x8)
    x8 = Lambda(lambda x: x[:, :, :75, :75, :])(x7)
    x9 = TimeDistributed(UpSampling2D(size=(4, 4)))(x8)
    x10 = TimeDistributed(ZeroPadding2D((2,2)))(x9)

    w1 = Concatenate(axis=4)([x10, y5, z7, zz])

    w2 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='sigmoid', name='cLSTM_w1')(w1)

    seq = Model(input_img, w2)

    seq.layers[1].trainable = False
    seq.layers[2].trainable = False
    seq.layers[3].trainable = False
    seq.layers[4].trainable = False
    seq.layers[5].trainable = False
    seq.layers[6].trainable = False
    seq.layers[7].trainable = False
    seq.layers[8].trainable = False
    seq.layers[9].trainable = False
    seq.layers[10].trainable = False
    seq.layers[11].trainable = False
    seq.layers[12].trainable = False
    seq.layers[13].trainable = False
    # seq.layers[14].trainable = False
    # seq.layers[15].trainable = False
    # seq.layers[16].trainable = False

    # for i in range(1, 16):
    #     print(seq.layers[i].layer.name)

    return seq


#  function to return the lstm network with skip connections
def network_definition_rgb_functional_skip(channel_number, full_image_size, reduced_size, n_frames, n_gt_frames):

    input_img = Input(shape=(n_gt_frames, full_image_size, full_image_size, channel_number))
    x1 = Conv3D(filters=3, kernel_size=(3, 1, 1),
                strides=(1, 2, 2), padding='same',
                data_format='channels_last')(input_img)
    x1 = BatchNormalization()(x1)

    x2 = ConvLSTM2D(filters=8, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM1')(x1)
    x2 = BatchNormalization(axis=4)(x2)
    x2 = TimeDistributed(MaxPooling2D((2,2)))(x2)

    x2_1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3), strides=(4,4)))(x2)

    x3 = ConvLSTM2D(filters=16, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM2')(x2)
    x3 = BatchNormalization()(x3)
    x3 = TimeDistributed(MaxPooling2D((2,2), padding='same'))(x3)
    x3_1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3), strides=(2,2), padding='same'))(x3)

    x4 = ConvLSTM2D(filters=32, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM3')(x3)
    x4 = BatchNormalization()(x4)
    x4 = TimeDistributed(MaxPooling2D((2, 2), padding='same'))(x4)
    x4_1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3), strides=(1,1), padding='same'))(x4)

    x5 = ConvLSTM2D(filters=16, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM4')(x4)
    x5 = BatchNormalization()(x5)
    x5_1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3), strides=(1,1), padding='same'))(x5)
    # x5 = TimeDistributed(MaxPooling2D((2, 2), padding='same'))(x5)
    #
    x6 = ConvLSTM2D(filters=8, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM5')(x5)
    x6 = BatchNormalization()(x6)
    # x6 = TimeDistributed(MaxPooling2D((2, 2), padding='same'))(x6)

    # x7 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='sigmoid', name='cLSTM6')(x6)
    # x7 = BatchNormalization()(x7)

    x7 = Concatenate()([x6, x2_1])
    x7_1 = Concatenate()([x7, x3_1 ])
    x7_2 = Concatenate()([x7_1, x4_1 ])
    x7_3 = Concatenate()([x7_2, x5_1 ])

    x8 = Conv3D(filters=1, kernel_size=(3, 3, 3),
                padding='same', data_format='channels_last')(x7_3)
    x8 = BatchNormalization()(x8)

    seq = Model(input_img, x8)

    return seq


def network_definition_regressor(full_image_size, array_size, n_frames, n_gt_frames):

    def space_to_depth_30(x):
        return tf.space_to_depth(x, block_size=30)

    def space_to_depth_15(x):
        return tf.space_to_depth(x, block_size=15)

    def space_to_depth_25(x):
        return  tf.space_to_depth(x, block_size=3)


    channel_number = 3
    input_img = Input(shape=(n_gt_frames, full_image_size, full_image_size, channel_number))

    model_vgg16_conv = VGG16(weights='imagenet', include_top=False)

    conv1 = TimeDistributed(model_vgg16_conv.layers[1])(input_img)
    conv2 = TimeDistributed(model_vgg16_conv.layers[2])(conv1)
    conv3 = TimeDistributed(model_vgg16_conv.layers[3])(conv2)
    conv4 = TimeDistributed(model_vgg16_conv.layers[4])(conv3)
    conv5 = TimeDistributed(model_vgg16_conv.layers[5])(conv4)
    conv6 = TimeDistributed(model_vgg16_conv.layers[6])(conv5)
    conv7 = TimeDistributed(model_vgg16_conv.layers[7])(conv6)
    conv8 = TimeDistributed(model_vgg16_conv.layers[8])(conv7)
    conv9 = TimeDistributed(model_vgg16_conv.layers[9])(conv8)
    conv10 = TimeDistributed(model_vgg16_conv.layers[10])(conv9)  # pooling
    conv11 = TimeDistributed(model_vgg16_conv.layers[11])(conv10)
    # conv111 = TimeDistributed(Dropout(0.5))(conv11)
    conv12 = TimeDistributed(model_vgg16_conv.layers[12])(conv11)
    conv13 = TimeDistributed(model_vgg16_conv.layers[13])(conv12)

    zz = TimeDistributed(Conv2D(filters=1, kernel_size=(3,3),
                                padding='same', activation='relu',
                                name='conv1_zz'))(conv5)
    zz1 = BatchNormalization()(zz)
    zz1 = ConvLSTM2D(filters=1, kernel_size=(1, 1),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM_w1')(zz1)
    zz2 = TimeDistributed(Lambda(space_to_depth_zz))(zz1)
    # zz2 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
    #                 padding='same', return_sequences=True,
    #                 activation='relu', name='cLSTM_w1'))(zz1)
    zz4 = TimeDistributed(Lambda(space_to_depth_30))(zz1)
    # zz4 = TimeDistributed(Lambda(space_to_depth_25))(zz3)

    z2 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3),
                                padding='same', activation='relu',
                                name='conv1_z'))(conv9)
    z3 = BatchNormalization()(z2)
    z4 = TimeDistributed(Lambda(space_to_depth_z))(z3)
    z4 = ConvLSTM2D(filters=1, kernel_size=(3, 3),
                     padding='same', return_sequences=True,
                     activation='relu', name='cLSTM_z')(z3)
    z5 = TimeDistributed(Lambda(space_to_depth_15))(z4)
    # z6 = TimeDistributed(Lambda(space_to_depth_25))(z5)

    y1 = TimeDistributed(Conv2D(filters=1, kernel_size=(3, 3), padding='same', activation='relu', name='conv1_y'))(conv13)
    y2 = BatchNormalization()(y1)

    y4 = TimeDistributed(Lambda(space_to_depth_25))(y2)

    w1 = Concatenate(axis=4)([y4, z5, zz4])

    rr = ConvLSTM2D(filters=5, kernel_size=(3, 3),
                    padding='same', return_sequences=True,
                    activation='relu', name='cLSTM_y')(w1)

    w1 = Concatenate(axis=4)([y2, z4, zz2])

    w2 = TimeDistributed(Conv2D(filter=5, kernel_size=(3,3), padding='same', activation='relu', name='con2d_regress'))(w1)
    rr = TimeDistributed(Conv2D(filters=5,
                                kernel_size=(3, 3),
                                padding='same',
                                activation='relu',
                                name='con2d_regress',
                                kernel_initializer='lecun_normal'))(rr)

    seq = Model(input_img, rr)

    seq.layers[1].trainable = False
    seq.layers[2].trainable = False
    seq.layers[3].trainable = False
    seq.layers[4].trainable = False
    seq.layers[5].trainable = False
    seq.layers[6].trainable = False
    seq.layers[7].trainable = False
    seq.layers[8].trainable = False
    seq.layers[9].trainable = False
    seq.layers[10].trainable = False
    seq.layers[11].trainable = False
    seq.layers[12].trainable = False
    seq.layers[13].trainable = False
    # seq.layers[14].trainable = False
    # seq.layers[15].trainable = False
    # seq.layers[16].trainable = False

    return seq


def network_definition_refined_regressor(image_size, channel_number):

    input_img = Input(shape=(image_size, image_size, channel_number))

    x2 = Flatten()(input_img)
    x5 = Dense(1125)(x2)
    x5 = BatchNormalization()(x5)
    x5 = Dropout(0.5)(x5)
    # x6 = Dense(700)(x5)
    # x6 = BatchNormalization()(x6)
    # x6 = Dropout(0.5)(x6)
    # x6 = Dense(225)(x6)
    # x6 = BatchNormalization()(x6)
    # x6 = Dropout(0.5)(x6)
    x7 = Dense(1, activation='sigmoid')(x5)

    # x8 = LSTM(50, return_sequences=True)(x7)

    seq = Model(input_img, x7)

    return seq


def network_definition_refined_classifier(image_size, channel_number):

    input_img = Input(shape=(image_size, image_size, channel_number))

    x2 = Flatten()(input_img)
    x5 = Dense(1125, activation='relu')(x2)
    x5 = BatchNormalization()(x5)
    x5 = Dropout(0.5)(x5)
    # x6 = Dense(700)(x5)
    # x6 = BatchNormalization()(x6)
    # x6 = Dropout(0.5)(x6)
    # x6 = Dense(225)(x6)
    # x6 = BatchNormalization()(x6)
    # x6 = Dropout(0.5)(x6)
    x7 = Dense(4, activation='relu')(x5)

    # x8 = LSTM(50, return_sequences=True)(x7)

    seq = Model(input_img, x7)

    return seq


class Adam_accumulate(Optimizer):
    '''Adam accumulate optimizer.

    Default parameters follow those provided in the original paper. Wait for several mini-batch to update

    # Arguments
        lr: float >= 0. Learning rate.
        beta_1/beta_2: floats, 0 < beta < 1. Generally close to 1.
        epsilon: float >= 0. Fuzz factor.

    # References
        - [Adam - A Method for Stochastic Optimization](http://arxiv.org/abs/1412.6980v8)
    '''

    def __init__(self, lr=0.001, beta_1=0.9, beta_2=0.999,
                 epsilon=1e-8, accum_iters=10, **kwargs):
        super(Adam_accumulate, self).__init__(**kwargs)
        self.__dict__.update(locals())
        self.iterations = K.variable(0)
        self.lr = K.variable(lr)
        self.beta_1 = K.variable(beta_1)
        self.beta_2 = K.variable(beta_2)
        self.accum_iters = K.variable(accum_iters)

    # def get_updates(self, params, constraints, loss):
    def get_updates(self, loss, params):
        grads = self.get_gradients(loss, params)
        self.updates = [(self.iterations, self.iterations + 1)]

        t = self.iterations + 1
        lr_t = self.lr * K.sqrt(1. - K.pow(self.beta_2, t)) / (1. - K.pow(self.beta_1, t))

        ms = [K.variable(np.zeros(K.get_value(p).shape)) for p in params]
        vs = [K.variable(np.zeros(K.get_value(p).shape)) for p in params]
        gs = [K.variable(np.zeros(K.get_value(p).shape)) for p in params]
        self.weights = ms + vs

        for p, g, m, v, gg in zip(params, grads, ms, vs, gs):

            flag = K.equal(self.iterations % self.accum_iters, 0)
            flag = K.cast(flag, dtype='float32')

            gg_t = (1 - flag) * (gg + g)
            m_t = (self.beta_1 * m) + (1. - self.beta_1) * (gg + flag * g) / self.accum_iters
            v_t = (self.beta_2 * v) + (1. - self.beta_2) * K.square((gg + flag * g) / self.accum_iters)
            p_t = p - flag * lr_t * m_t / (K.sqrt(v_t) + self.epsilon)

            self.updates.append((m, flag * m_t + (1 - flag) * m))
            self.updates.append((v, flag * v_t + (1 - flag) * v))
            self.updates.append((gg, gg_t))

            new_p = p_t
            # apply constraints
            ### if p in constraints:
            ###     c = constraints[p]
            ###     new_p = c(new_p)
            self.updates.append((p, new_p))
        return self.updates



    def get_config(self):
        config = {'lr': float(K.get_value(self.lr)),
                  'beta_1': float(K.get_value(self.beta_1)),
                  'beta_2': float(K.get_value(self.beta_2)),
                  'epsilon': self.epsilon}
        base_config = super(Adam_accumulate, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))
