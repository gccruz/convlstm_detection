""" This script demonstrates the use of a convolutional LSTM network.
This network is used to predict the next frame of a movie resulting from the predictions of vgg network.
"""
from keras.callbacks import ModelCheckpoint, \
                            TensorBoard
                            # LearningRateScheduler, \
                            # Callback
from keras.models import load_model
import numpy as np
import os
import time
from lstm_utils import network_mixed_multiclear
from lstm_data_generator import load_dataset_strided
import keras.backend as K
from keras.optimizers import adadelta
import sys
import glob
from keras.metrics import binary_accuracy

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = '0'

_EPSILON = K.epsilon()

batch_size = 1
n_frames = 60
gt_time_steps = 6
n_frames_of_txt_file = 60
img_size = 600
reduced_size = 300
channel_number = 3
stage = 'test'
label = 'ref163'
n_epoch = 50

sd=[]

dataset_dir = '/home/gcx/sequences/persistent_dataset/'

# Load previous model
model_to_load = glob.glob('/home/gcx/repositories/auto-encoder/lstm/models/' + label + '/*h5')

if len(model_to_load) > 1:
   sys.exit('Multiple files in folder. Do not know what to choose. Exiting!')
elif len(model_to_load) == 0:
   sys.exit('No model file found. Exiting!')

print('loading model: ', model_to_load)
seq = load_model(model_to_load[0])

# seq = network_mixed_multiclear(channel_number=channel_number,
#                                           full_image_size=img_size,
#                                           reduced_size=reduced_size,
#                                           n_frames=n_frames,
#                                           n_gt_frames=gt_time_steps)

seq.compile(loss='binary_crossentropy', optimizer='adadelta', metrics=[binary_accuracy])

print(seq.summary())
print('loaded model: ', model_to_load)

# set the name for the files
current_date = time.strftime('%Y%m%d')
current_time = time.strftime('%H%M')
model_fn = 'models/lstm_rgb_' + label + '.ep{epoch:02d}-ls{val_loss:.5f}' + '_' + str(reduced_size) \
           + '_' + str(img_size) + '_' + str(n_frames) + current_date + '_' + current_time + '.h5'

# Define flags for model weight saving
cb_modelCheckpoint = ModelCheckpoint(model_fn, monitor='val_loss',
                                     verbose=0, save_best_only=True,
                                     save_weights_only=False, mode='min', period=1)

# path that was used only with positive samples
# Specify the sequence to use
# file_sequence_list = np.genfromtxt('neg_n_pos_seq/correct_' + stage + '_dataset_3_' + str(n_frames_of_txt_file) + '.txt', dtype='str')
val_file_sequence_list = np.genfromtxt('neg_n_pos_seq/correct_' + stage + '_dataset_3_' + str(n_frames_of_txt_file) + '.txt', dtype='str')
# val_file_sequence_list = np.genfromtxt('neg_n_pos_seq/correct_' + 'test_wake' + '_dataset_3_' + str(n_frames_of_txt_file) + '.txt', dtype='str')


# train_set = load_dataset_strided(dataset_dir, batch_size,
#                                  img_size=img_size, reduced_size=reduced_size,
#                                  n_frames=n_frames, stage='train',
#                                  channel_number=channel_number, gt_time_steps=gt_time_steps,
#                                  n_frames_txt_file = n_frames_of_txt_file)

# validation_set = load_dataset_strided(dataset_dir, batch_size,
#                                       img_size=img_size, reduced_size=reduced_size,
#                                       n_frames=n_frames, stage='test',
#                                       channel_number=channel_number, gt_time_steps=gt_time_steps,
#                                       n_frames_txt_file=n_frames_of_txt_file)

wake_set = load_dataset_strided(dataset_dir, batch_size,
                                      img_size=img_size, reduced_size=reduced_size,
                                      n_frames=n_frames, stage='test_wake',
                                      channel_number=channel_number, gt_time_steps=gt_time_steps,
                                      n_frames_txt_file=n_frames_of_txt_file)


print('len(file_sequence_list) ', len(val_file_sequence_list))
nb_steps_per_epoch = len(val_file_sequence_list)/float(batch_size) # complete number of files to try
print('nb_steps_per_epoch', nb_steps_per_epoch)
# nb_validation_steps = 1000. / batch_size

# Train the network
# seq.fit_generator(train_set,
#                   validation_data=validation_set,
#                   steps_per_epoch=nb_steps_per_epoch,
#                   nb_epoch=n_epoch,
#                   callbacks=[TensorBoard(log_dir='/tmp/lstm_rgb_' + label),
#                              cb_modelCheckpoint],
#                              # history,
#                              # lrate],
#                   validation_steps=nb_validation_steps,
#                   workers=6,
#                   use_multiprocessing=True,
#                   max_queue_size=10)


print seq.evaluate_generator(wake_set,
                       steps=nb_steps_per_epoch,
                       max_queue_size=10,
                       workers=1,
                       use_multiprocessing=False)
print seq.metrics_names
