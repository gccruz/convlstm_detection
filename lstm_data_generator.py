import numpy as np
import sys
import cv2
import os
import glob


# function to load the X and Y data to be processed
def lstm_image_generator(file_list,
                    n_frames, channel_number, img_size , reduced_size, stage, gt_time_steps,
                    to_grayscale=False):
    n_samples = 1  # Attention: this variable actually is dumb.
    #  This resulted from adapting the code to load many sample at once, into data generator

    x_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/x_' + stage + '/'
    y_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/y_' + stage + '/'

    while True:
        x = np.zeros(( n_frames, img_size, img_size, channel_number), dtype=np.float)
        y = np.zeros(( n_frames, reduced_size, reduced_size, 1),
                     dtype=np.float)

        # if stage == 'test':
        #     label = np.zeros(1, dtype=object)

        i = np.random.randint(0, len(file_list))

        # Load samples for X
        # for i in range(n_samples):

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant. Exiting image generator')
        else:
            for j in range(n_frames):
                if channel_number == 1:  # If we are considering gray-scale images
                    try:
                        temp_img = cv2.cvtColor(cv2.imread(x_data_dir + current_file_list[j]), cv2.COLOR_BGR2GRAY)
                        temp_img /= 255
                        x[j, :, :, 0] = temp_img
                    except:
                        sys.exc_info()[0]
                        sys.exit('could not load image: Exiting image generator')

                elif channel_number == 3:  # If we consider RGB images
                    try:
                        temp_img = cv2.imread(x_data_dir + current_file_list[j])
                        temp_img = cv2.resize(temp_img, (img_size, img_size))
                        temp_img =  temp_img / 255.0
                        x[j, :, :, :] = temp_img
                    except:
                        sys.exc_info()[0]
                        sys.exit('could not load image: Exiting image generator')

                elif channel_number > 3:  # If we consider multi-channel images
                    # try:
                    temp_img = np.load(x_data_dir + current_file_list[j][:-4] + '.npy')
                    # print('temp_img.max(): ', temp_img.max())
                    temp_img = temp_img / temp_img.max()
                    x[j, :, :, :] = temp_img
                    # except:
                    #     sys.exit('could not load npy: Exiting image generator')

                else:
                    sys.exit('incorrect number of channels in X!!')

        # Load data for Y
        if len(file_list) < n_samples:
            sys.exit('not enough sample in the designated set. Exiting script!')

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for j in range(n_frames):
                if y.shape[-1] == 1:
                    # try:
                    temp_img = cv2.imread(y_data_dir + current_file_list[j][:-4] + '.tiff')
                    temp_img = cv2.cvtColor(temp_img, cv2.COLOR_BGR2GRAY)
                    temp_img /= 255
                    y[j, :, :, 0] = cv2.resize(temp_img,
                                               (reduced_size, reduced_size),
                                               interpolation=cv2.INTER_NEAREST)

                else:

                    sys.exit('incorrect number of channels in y!!')

        # compute the stride
        stride_for_y = int(n_frames/gt_time_steps)
        idx_to_get = np.arange(1, n_frames, stride_for_y)

        # if stage == 'train':
        yield (x, np.take(y, idx_to_get, axis=0))


# function to load the X and Y data to be processed WITH DATA augmentation
def lstm_image_generator_dt_aug(file_list,
                    n_frames, channel_number, img_size , reduced_size, stage, gt_time_steps,
                    to_grayscale=False):
    n_samples = 1  # Attention: this variable actually is dumb.
    #  This resulted from adapting the code to load many sample at once, into data generator

    # distortion limits
    lower_limit = 0.6
    upper_limit = 1.4

    x_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/x_' + stage + '/'
    y_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/y_' + stage + '/'

    while True:
        x = np.zeros(( n_frames, img_size, img_size, channel_number),
                     dtype=np.float32)
        y = np.zeros(( n_frames, reduced_size, reduced_size, 1),
                     dtype=np.float32)

        # if stage == 'test':
        #     label = np.zeros(1, dtype=object)

        i = np.random.randint(0, len(file_list))

        # Load samples for X
        # for i in range(n_samples):

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant. Exiting image generator')
        else:
            for j in range(n_frames):
                if channel_number == 1:  # If we are considering gray-scale images
                    try:
                        temp_img = (np.asanyarray(cv2.cvtColor(cv2.imread(x_data_dir + current_file_list[j]), cv2.COLOR_BGR2GRAY), np.float32))
                        temp_img /= 255

                        x[j, :, :, 0] = np.asanyarray(temp_img, np.float32)
                    except:
                        sys.exc_info()[0]
                        sys.exit('could not load image: Exiting image generator')

                elif channel_number == 3:  # If we consider RGB images
                    try:
                        # print x_data_dir + current_file_list[j]
                        temp_img = np.asanyarray(cv2.resize(cv2.imread(x_data_dir + current_file_list[j]), (img_size, img_size)), np.float32)

                        # Distortions
                        # convert to HSV
                        temp_img = cv2.cvtColor(temp_img, cv2.COLOR_BGR2HSV)
                        # distort the intensity
                        v_distortion = (lower_limit + np.random.random() * (upper_limit - lower_limit))
                        intm = (v_distortion * np.float32(temp_img[:, :, 2]))
                        temp_img[:, :, 2] = np.uint8(np.clip(intm, 0., 255.))
                        intm = None

                        # distort the saturation
                        s_distortion = (lower_limit + np.random.random() * (upper_limit - lower_limit))
                        satm = (s_distortion * np.float32(temp_img[:, :, 1]))
                        temp_img[:, :, 1] = np.uint8(np.clip(satm, 0., 255.))
                        satm = None

                        # distort the hue
                        h_distortion = (lower_limit + np.random.random() * (upper_limit - lower_limit))
                        huem = (h_distortion * np.float32(temp_img[:, :, 0]))
                        temp_img[:, :, 0] = np.uint8(np.clip(huem, 0., 255.))
                        huem = None

                        # convert back to BGR
                        temp_img = cv2.cvtColor(temp_img, cv2.COLOR_HSV2BGR)
                        # cv2.imshow('img', temp_img), cv2.waitKey(1)
                        temp_img = cv2.resize(temp_img, (img_size, img_size))
                        temp_img =  temp_img / 255.0


                        x[j, :, :, :] = temp_img

                    except:
                        sys.exc_info()[0]
                        sys.exit('could not load image: Exiting image generator')

                elif channel_number > 3:  # If we consider multi-channel images
                    # try:
                    temp_img = np.load(x_data_dir + current_file_list[j][:-4] + '.npy')
                    # print('temp_img.max(): ', temp_img.max())
                    temp_img = temp_img / temp_img.max()
                    x[j, :, :, :] = temp_img
                    # except:
                    #     sys.exit('could not load npy: Exiting image generator')

                else:
                    sys.exit('incorrect number of channels in X!!')

        # Load data for Y
        if len(file_list) < n_samples:
            sys.exit('not enough sample in the designated set. Exiting script!')

        # for i in range(n_samples):

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for j in range(n_frames):
                if y.shape[-1] == 1:
                    # try:

                    temp_img = cv2.imread(y_data_dir + current_file_list[j][:-4] + '.tiff')
                    temp_img = cv2.cvtColor(temp_img, cv2.COLOR_BGR2GRAY)
                    temp_img /= 255
                    y[j, :, :, 0] = cv2.resize(temp_img,
                                               (reduced_size, reduced_size),
                                               interpolation=cv2.INTER_NEAREST)

                else:

                    sys.exit('incorrect number of channels in y!!')

        # compute the stride
        stride_for_y = int(n_frames/gt_time_steps)
        idx_to_get = np.arange(1, n_frames, stride_for_y)
        # z = np.take(y, idx_to_get, axis=0)

        # if stage == 'train':
        yield (np.asanyarray(x, np.float32), np.asanyarray(np.take(y, idx_to_get, axis=0), np.float32))
        # yield (x, np.take(y, idx_to_get, axis=0))
        # else:  np.asanyarray(x, np.float32)
        #     yield (x, y, label)# function to load the X and Y data to be processed WITH DATA augmentation


def lstm_image_generator_strided(file_list, dataset_folder,
                    n_frames, channel_number, img_size , reduced_size, stage, gt_time_steps):
    n_samples = 1  # Attention: this variable actually is dumb.
    #  This resulted from adapting the code to load many sample at once, into data generator

    # distortion limits
    lower_limit = 0.8
    upper_limit = 1.2


    # version of dataset that has some incorrections
    # x_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/x_' + stage + '/'
    # y_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/y_' + stage + '/'
    # corrected version of dataset
    x_data_dir = dataset_folder + stage + '/x_' + stage + '/'
    y_data_dir = dataset_folder + stage + '/y_' + stage + '/'

    # compute the stride
    temporal_stride = int(n_frames / gt_time_steps)


    print('sampled instants:')
    for j in range(gt_time_steps):
        print(j * temporal_stride)

    while True:
        x = np.zeros((gt_time_steps, img_size, img_size, channel_number),
                     dtype=np.float32)
        y = np.zeros((gt_time_steps, reduced_size, reduced_size, 1),
                     dtype=np.float32)

        # if stage == 'test':
        #     label = np.zeros(1, dtype=object)

        i = np.random.randint(0, len(file_list))

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant. Exiting image generator')
        else:
            for j in range(gt_time_steps):
                if channel_number == 1:  # If we are considering gray-scale images
                    # try:
                    if os.path.isfile(x_data_dir + current_file_list[j * temporal_stride]):
                        temp_img = np.asanyarray(cv2.cvtColor(
                            cv2.imread(x_data_dir + current_file_list[j * temporal_stride]),
                            cv2.COLOR_BGR2GRAY), np.float32)
                        temp_img /= 255
                    else:
                        # print(x_data_dir + current_file_list[j * temporal_stride])
                        sys.exit('problem loading file')

                    x[j, :, :, 0] = (np.asanyarray(temp_img, np.float32))
                    # except:
                    #     sys.exc_info()[0]
                    #     sys.exit('could not load image: Exiting image generator')

                elif channel_number == 3:  # If we consider RGB images
                    # try:
                    # print(x_data_dir + current_file_list[j * temporal_stride])

                    # if os.path.isfile(x_data_dir + current_file_list[j * temporal_stride]):
                    # print('file name', x_data_dir + current_file_list[j * temporal_stride])
                    try:
                        temp_img = np.asanyarray(cv2.imread(x_data_dir + current_file_list[j * temporal_stride]),
                                np.float32)
                    except:
                        print('could not load: ', x_data_dir + current_file_list[j * temporal_stride])
                        sys.error('error loading file')
                        break

                    # else:
                    #     print(x_data_dir + current_file_list[j * temporal_stride])
                    #     sys.exit('problem loading file')

                    # Distortions
                    # convert to HSV
                    temp_img = cv2.cvtColor(temp_img, cv2.COLOR_BGR2HSV)
                    # distort the intensity
                    v_distortion = (lower_limit + np.random.random() * (upper_limit - lower_limit))
                    intm = (v_distortion * np.float32(temp_img[:, :, 2]))
                    temp_img[:, :, 2] = np.uint8(np.clip(intm, 0., 255.))
                    del intm

                    # distort the saturation
                    s_distortion = (lower_limit + np.random.random() * (upper_limit - lower_limit))
                    satm = (s_distortion * np.float32(temp_img[:, :, 1]))
                    temp_img[:, :, 1] = np.uint8(np.clip(satm, 0., 255.))
                    del satm

                    # distort the hue
                    h_distortion = (lower_limit + np.random.random() * (upper_limit - lower_limit))
                    huem = (h_distortion * np.float32(temp_img[:, :, 0]))
                    temp_img[:, :, 0] = np.uint8(np.clip(huem, 0., 255.))
                    del huem

                    # convert back to BGR
                    temp_img = cv2.cvtColor(temp_img, cv2.COLOR_HSV2BGR)
                    temp_img = cv2.resize(temp_img, (img_size, img_size))
                    temp_img = temp_img / 255.0

                    x[j, :, :, :] = temp_img

                    # except:
                    #     sys.exc_info()[0]
                    #     sys.exit('could not load image: Exiting image generator')

                else:
                    sys.exit('incorrect number of channels in X!!')

        # Load data for Y
        if len(file_list) < n_samples:
            sys.exit('not enough sample in the designated set. Exiting script!')

        # for i in range(n_samples):

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for j in range(gt_time_steps):
                if y.shape[-1] == 1:
                    # try:
                    temp_img = cv2.imread(
                        y_data_dir + current_file_list[j * temporal_stride][:-4] + '.tiff')
                    temp_img = cv2.cvtColor(temp_img, cv2.COLOR_BGR2GRAY)
                    temp_img /= 255
                    y[j, :, :, 0] = cv2.resize(temp_img,
                                               (reduced_size, reduced_size),
                                               interpolation=cv2.INTER_NEAREST)
                    # print('GT: ', y_data_dir + current_file_list[j * temporal_stride][:-4] + '.tiff')
                else:

                    sys.exit('incorrect number of channels in y!!')

        yield (np.asanyarray(x, np.float32), np.asanyarray(y, np.float32))

def lstm_image_generator_strided_flatten(file_list, dataset_folder,
                    n_frames, channel_number, img_size , reduced_size, stage, gt_time_steps,
                    to_grayscale=False):
    n_samples = 1  # Attention: this variable actually is dumb.
    #  This resulted from adapting the code to load many sample at once, into data generator

    # distortion limits
    lower_limit = 0.8
    upper_limit = 1.2
    n_frames_txt = 40

    # version of dataset that has some incorrections
    # x_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/x_' + stage + '/'
    # y_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/y_' + stage + '/'
    # corrected version of dataset
    x_data_dir = dataset_folder + stage + '/x_' + stage + '/'
    y_data_dir = dataset_folder + stage + '/y_' + stage + '/'

    # compute the stride
    temporal_stride = int(n_frames / gt_time_steps)
    # idx_to_get = np.arange(1, n_frames, temporal_stride)

    while True:
        x = np.zeros(( gt_time_steps, img_size, img_size, channel_number),
                     dtype=np.float32)
        y = np.zeros((gt_time_steps, reduced_size * reduced_size),
                     dtype=np.float32)

        # if stage == 'test':
        #     label = np.zeros(1, dtype=object)

        i = np.random.randint(0, len(file_list))

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant. Exiting image generator')
        else:
            for j in range(gt_time_steps):
                if channel_number == 1:  # If we are considering gray-scale images
                    # try:
                    if os.path.isfile(x_data_dir + current_file_list[j * temporal_stride]):
                        temp_img = np.asanyarray(cv2.cvtColor(
                            cv2.imread(x_data_dir + current_file_list[j * temporal_stride]),
                            cv2.COLOR_BGR2GRAY), np.float32)
                        temp_img /= 255
                    else:
                        # print(x_data_dir + current_file_list[j * temporal_stride])
                        sys.exit('problem loading file')

                    x[j, :, :, 0] = (np.asanyarray(temp_img, np.float32))
                    # except:
                    #     sys.exc_info()[0]
                    #     sys.exit('could not load image: Exiting image generator')

                elif channel_number == 3:  # If we consider RGB images
                    # try:
                    # print(x_data_dir + current_file_list[j * temporal_stride])

                    # if os.path.isfile(x_data_dir + current_file_list[j * temporal_stride]):
                    # print('file name', x_data_dir + current_file_list[j * temporal_stride])
                    try:
                        temp_img = np.asanyarray(cv2.imread(x_data_dir + current_file_list[j * temporal_stride]),
                                np.float32)
                    except:
                        print('could not load: ', x_data_dir + current_file_list[j * temporal_stride])
                        sys.error('error loading file')
                        break

                    # else:
                    #     print(x_data_dir + current_file_list[j * temporal_stride])
                    #     sys.exit('problem loading file')

                    # Distortions
                    # convert to HSV
                    temp_img = cv2.cvtColor(temp_img, cv2.COLOR_BGR2HSV)
                    # distort the intensity
                    v_distortion = (lower_limit + np.random.random() * (upper_limit - lower_limit))
                    intm = (v_distortion * np.float32(temp_img[:, :, 2]))
                    temp_img[:, :, 2] = np.uint8(np.clip(intm, 0., 255.))
                    del intm

                    # distort the saturation
                    s_distortion = (lower_limit + np.random.random() * (upper_limit - lower_limit))
                    satm = (s_distortion * np.float32(temp_img[:, :, 1]))
                    temp_img[:, :, 1] = np.uint8(np.clip(satm, 0., 255.))
                    del satm

                    # distort the hue
                    h_distortion = (lower_limit + np.random.random() * (upper_limit - lower_limit))
                    huem = (h_distortion * np.float32(temp_img[:, :, 0]))
                    temp_img[:, :, 0] = np.uint8(np.clip(huem, 0., 255.))
                    del huem

                    # convert back to BGR
                    temp_img = cv2.cvtColor(temp_img, cv2.COLOR_HSV2BGR)
                    temp_img = cv2.resize(temp_img, (img_size, img_size))
                    temp_img = temp_img / 255.0

                    x[j, :, :, :] = temp_img

                    # except:
                    #     sys.exc_info()[0]
                    #     sys.exit('could not load image: Exiting image generator')

                else:
                    sys.exit('incorrect number of channels in X!!')

        # Load data for Y
        if len(file_list) < n_samples:
            sys.exit('not enough sample in the designated set. Exiting script!')

        # for i in range(n_samples):

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for j in range(gt_time_steps):
                # try:
                temp_img = cv2.imread(
                    y_data_dir + current_file_list[j * temporal_stride][:-4] + '.tiff')
                temp_img = cv2.cvtColor(temp_img, cv2.COLOR_BGR2GRAY)
                temp_img /= 255
                y[j, :] = np.reshape(cv2.resize(temp_img, (reduced_size, reduced_size),
                                           interpolation=cv2.INTER_NEAREST), (1, reduced_size * reduced_size))

        yield (np.asanyarray(x, np.float32), np.asanyarray(y, np.float32))


def lstm_image_generator_strided_flatten_reshaped(file_list, dataset_folder,
                    n_frames, channel_number, img_size , reduced_size, stage, gt_time_steps,
                    to_grayscale=False):
    n_samples = 1  # Attention: this variable actually is dumb.
    #  This resulted from adapting the code to load many sample at once, into data generator

    # distortion limits
    lower_limit = 0.8
    upper_limit = 1.2
    n_frames_txt = 40

    # version of dataset that has some incorrections
    # x_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/x_' + stage + '/'
    # y_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/y_' + stage + '/'
    # corrected version of dataset
    x_data_dir = dataset_folder + stage + '/x_' + stage + '/'
    y_data_dir = dataset_folder + stage + '/y_' + stage + '/'

    # compute the stride
    temporal_stride = int(n_frames / gt_time_steps)
    # idx_to_get = np.arange(1, n_frames, temporal_stride)

    while True:
        x = np.zeros(( gt_time_steps, img_size, img_size, channel_number),
                     dtype=np.float32)
        y = np.zeros((gt_time_steps, reduced_size, reduced_size),
                     dtype=np.float32)

        # if stage == 'test':
        #     label = np.zeros(1, dtype=object)

        i = np.random.randint(0, len(file_list))

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant. Exiting image generator')
        else:
            for j in range(gt_time_steps):
                if channel_number == 1:  # If we are considering gray-scale images
                    # try:
                    if os.path.isfile(x_data_dir + current_file_list[j * temporal_stride]):
                        temp_img = np.asanyarray(cv2.cvtColor(
                            cv2.imread(x_data_dir + current_file_list[j * temporal_stride]),
                            cv2.COLOR_BGR2GRAY), np.float32)
                        temp_img /= 255
                    else:
                        # print(x_data_dir + current_file_list[j * temporal_stride])
                        sys.exit('problem loading file')

                    x[j, :, :, 0] = (np.asanyarray(temp_img, np.float32))
                    # except:
                    #     sys.exc_info()[0]
                    #     sys.exit('could not load image: Exiting image generator')

                elif channel_number == 3:  # If we consider RGB images
                    # try:
                    # print(x_data_dir + current_file_list[j * temporal_stride])

                    # if os.path.isfile(x_data_dir + current_file_list[j * temporal_stride]):
                    # print('file name', x_data_dir + current_file_list[j * temporal_stride])
                    try:
                        temp_img = np.asanyarray(cv2.imread(x_data_dir + current_file_list[j * temporal_stride]),
                                np.float32)
                    except:
                        print('could not load: ', x_data_dir + current_file_list[j * temporal_stride])
                        sys.error('error loading file')
                        break

                    # else:
                    #     print(x_data_dir + current_file_list[j * temporal_stride])
                    #     sys.exit('problem loading file')

                    # Distortions
                    # convert to HSV
                    temp_img = cv2.cvtColor(temp_img, cv2.COLOR_BGR2HSV)
                    # distort the intensity
                    v_distortion = (lower_limit + np.random.random() * (upper_limit - lower_limit))
                    intm = (v_distortion * np.float32(temp_img[:, :, 2]))
                    temp_img[:, :, 2] = np.uint8(np.clip(intm, 0., 255.))
                    del intm

                    # distort the saturation
                    s_distortion = (lower_limit + np.random.random() * (upper_limit - lower_limit))
                    satm = (s_distortion * np.float32(temp_img[:, :, 1]))
                    temp_img[:, :, 1] = np.uint8(np.clip(satm, 0., 255.))
                    del satm

                    # distort the hue
                    h_distortion = (lower_limit + np.random.random() * (upper_limit - lower_limit))
                    huem = (h_distortion * np.float32(temp_img[:, :, 0]))
                    temp_img[:, :, 0] = np.uint8(np.clip(huem, 0., 255.))
                    del huem

                    # convert back to BGR
                    temp_img = cv2.cvtColor(temp_img, cv2.COLOR_HSV2BGR)
                    temp_img = cv2.resize(temp_img, (img_size, img_size))
                    temp_img = temp_img / 255.0

                    x[j, :, :, :] = temp_img

                    # except:
                    #     sys.exc_info()[0]
                    #     sys.exit('could not load image: Exiting image generator')

                else:
                    sys.exit('incorrect number of channels in X!!')

        # Load data for Y
        if len(file_list) < n_samples:
            sys.exit('not enough sample in the designated set. Exiting script!')

        # for i in range(n_samples):

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for j in range(gt_time_steps):
                # try:
                temp_img = cv2.imread(
                    y_data_dir + current_file_list[j * temporal_stride][:-4] + '.tiff')
                temp_img = cv2.cvtColor(temp_img, cv2.COLOR_BGR2GRAY)
                temp_img /= 255
                y[j, :, :] = np.reshape(cv2.resize(temp_img, (reduced_size, reduced_size),
                                           interpolation=cv2.INTER_NEAREST), (1, reduced_size, reduced_size))

        yield (np.asanyarray(x, np.float32), np.asanyarray(y, np.float32))


def lstm_image_generator_not_distorted(file_list,
                    n_frames, channel_number, img_size , reduced_size, stage, gt_time_steps,
                    to_grayscale=False):
    n_samples = 1  # Attention: this variable actually is dumb.
    #  This resulted from adapting the code to load many sample at once, into data generator


    # version of dataset that has some incorrections
    # x_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/x_' + stage + '/'
    # y_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/y_' + stage + '/'
    # corrected version of dataset
    x_data_dir = '/home/gcx/sequences/correct_dataset/' + stage + '/x_' + stage + '/'
    y_data_dir = '/home/gcx/sequences/correct_dataset/' + stage + '/y_' + stage + '/'

    # compute the stride
    temporal_stride = int(n_frames / gt_time_steps)
    idx_to_get = np.arange(1, n_frames, temporal_stride)
    strings_to_save = []

    while True:
        x = np.zeros(( gt_time_steps, img_size, img_size, channel_number),
                     dtype=np.float32)
        y = np.zeros(( gt_time_steps, reduced_size, reduced_size, 1),
                     dtype=np.float32)

        # if stage == 'test':
        #     label = np.zeros(1, dtype=object)

        i = np.random.randint(0, len(file_list))

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant. Exiting image generator')
        else:
            for j in range(gt_time_steps):
                if channel_number == 1:  # If we are considering gray-scale images
                    # try:
                    if os.path.isfile(x_data_dir + current_file_list[j * temporal_stride]):
                        temp_img = np.asanyarray(cv2.cvtColor(
                            cv2.imread(x_data_dir + current_file_list[j * temporal_stride]),
                            cv2.COLOR_BGR2GRAY), np.float32)
                        temp_img /= 255
                    else:
                        # print(x_data_dir + current_file_list[j * temporal_stride])
                        sys.exit('problem loading file')

                    x[j, :, :, 0] = (np.asanyarray(temp_img, np.float32))
                    # except:
                    #     sys.exc_info()[0]
                    #     sys.exit('could not load image: Exiting image generator')

                elif channel_number == 3:  # If we consider RGB images

                    temp_img = np.asanyarray(cv2.imread(x_data_dir + current_file_list[j * temporal_stride]),
                                np.float32)
                    strings_to_save.append(x_data_dir + current_file_list[j * temporal_stride])

                    # convert back to BGR
                    temp_img = cv2.cvtColor(temp_img, cv2.COLOR_HSV2BGR)
                    temp_img = cv2.resize(temp_img, (img_size, img_size))
                    temp_img = temp_img / 255.0

                    x[j, :, :, :] = temp_img

                else:
                    sys.exit('incorrect number of channels in X!!')

        # Load data for Y
        if len(file_list) < n_samples:
            sys.exit('not enough sample in the designated set. Exiting script!')

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for j in range(gt_time_steps):
                if y.shape[-1] == 1:
                    # try:
                    temp_img = cv2.imread(
                        y_data_dir + current_file_list[j * temporal_stride][:-4] + '.tiff')
                    temp_img = cv2.cvtColor(temp_img, cv2.COLOR_BGR2GRAY)
                    temp_img /= 255
                    y[j, :, :, 0] = cv2.resize(temp_img,
                                               (reduced_size, reduced_size),
                                               interpolation=cv2.INTER_NEAREST)

                else:

                    sys.exit('incorrect number of channels in y!!')

        # compute the stride
        # stride_for_y = int(n_frames/gt_time_steps)
        # idx_to_get = np.arange(1, n_frames, stride_for_y)
        # z = np.take(y, idx_to_get, axis=0)

        # if stage == 'train':
        # yield (np.asanyarray(x, np.float32), np.asanyarray(np.take(y, idx_to_get, axis=0), np.float32))
        yield (np.asanyarray(x, np.float32), np.asanyarray(y, np.float32), strings_to_save)
        # yield (x, np.take(y, idx_to_get, axis=0))
        # else:  np.asanyarray(x, np.float32)
        #     yield (x, y, label)


def lstm_img_gen_regressor(file_list,
                    n_frames, channel_number, img_size , reduced_size, stage, gt_time_steps,
                    to_grayscale=False):

    n_samples = 1  # Attention: this variable actually is dumb.
    #  This resulted from adapting the code to load many sample at once, into data generator

    # distortion limits
    lower_limit = 0.6
    upper_limit = 1.4

    # version of dataset that has some incorrections
    # x_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/x_' + stage + '/'
    # y_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/y_' + stage + '/'
    # corrected version of dataset
    x_data_dir = '/home/gcx/datasets/labelling/' + stage + '/x_' + stage + '/'
    y_data_dir = '/home/gcx/datasets/labelling/' + stage + '/label_' + stage + '/'

    # compute the stride
    temporal_stride = int(n_frames / gt_time_steps)
    idx_to_get = np.arange(1, n_frames, temporal_stride)

    while True:
        x = np.zeros((gt_time_steps, reduced_size, reduced_size, 1),
                     dtype=np.float32)
        # todo change size of y
        y = np.zeros((gt_time_steps, 10*5),
                     dtype=np.float32)

        i = np.random.randint(0, len(file_list))

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant. Exiting image generator')
        else:
            for j in range(gt_time_steps):

                if channel_number == 1:  # If we consider prediction maps

                    file_number = format(int(current_file_list[j * temporal_stride][4:-5]), '06d')

                    # temp_img = np.asanyarray(cv2.imread(x_data_dir + current_file_list[j * temporal_stride]), np.float32)
                    file_name = x_data_dir + current_file_list[j * temporal_stride][2] + '/tiff/'\
                                + file_number + '.tiff'


                    try:
                        temp_img = np.asanyarray(
                                            cv2.cvtColor(
                                                cv2.imread(file_name), cv2.COLOR_BGR2GRAY),
                                    np.float32)/ 255.
                        # print(temp_img.shape)
                    except:
                        print(file_name)
                        print('cloud not load image"')
                        break

                    x[j, :, :, :] = np.reshape(temp_img, (temp_img.shape[0], temp_img.shape[0], 1))

                else:
                    sys.exit('incorrect number of channels in X!!')

        # Load data for Y
        if len(file_list) < n_samples:
            sys.exit('not enough sample in the designated set. Exiting script!')

        current_file_list = file_list[i, :]

        if len(current_file_list) < n_frames:
            sys.exit('not enough samples per time instant . Exiting script!')
        else:
            for j in range(gt_time_steps):


                # todo reformat gt to have timex75x75x5 tensor
                gt = np.load(y_data_dir + current_file_list[j * temporal_stride][2] + '/' +
                            current_file_list[j * temporal_stride][4:-5] + '.npy')

                y[j, :] = np.reshape(gt, (2*5))

        yield (np.asanyarray(x, np.float32), np.asanyarray(y, np.float32))


def lstm_image_generator_cropped_regressor(file_list,  dataset_folder, img_size, numb_channels):

    # distortion limits
    lower_limit = 0.6
    upper_limit = 1.4

    # version of dataset that has some incorrections
    # x_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/x_' + stage + '/'
    # y_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/y_' + stage + '/'
    # corrected version of dataset
    # x_data_dir = '/home/gcx/datasets/labelling/' + stage + '/x_' + stage + '/'
    # y_data_dir = '/home/gcx/datasets/labelling/' + stage + '/label_' + stage + '/'

    while True:
        # x = np.zeros((12, 12, 256), dtype=np.float32)

        # y = np.zeros((4,1), dtype=np.float32)

        i = np.random.randint(0, len(file_list))

        current_file = os.path.split(file_list[i])[1]

        # if channel_number == 256:  # If we consider prediction maps

        # temp_img = np.asanyarray(cv2.imread(x_data_dir + current_file_list[j * temporal_stride]), np.float32)
        file_name = dataset_folder + '/features/' + current_file

        try:
            # print('file_name', file_name)
            x = np.reshape(np.load(file_name), (img_size, img_size, numb_channels)) / 255.
            # temp_img = np.asanyarray(
            #                     cv2.cvtColor(
            #                         cv2.imread(file_name), cv2.COLOR_BGR2GRAY),
            #             np.float32)/ 255.
            # print(temp_img.shape)

        except:
            print('could not load image')
            print(file_name)
            sys.error('cloud not load image"')
            break

        # Load data for Y
        file_name = dataset_folder + '/gt/' + current_file

        try:
            gt = np.load(file_name)

            y = np.reshape(gt, 5)
            y = y[:4] / 50.

            # print('y: ', y)
        except:
            print('file name: ', file_name)
            sys.error('could not load ground truth')

        # print(x.max())
        # print(y.max())

        yield (np.asanyarray(x, np.float32), np.asanyarray(y, np.float32))


def lstm_image_generator_cropped_classifier(file_list,  dataset_folder, img_size, numb_channels):

    # distortion limits
    lower_limit = 0.6
    upper_limit = 1.4

    # version of dataset that has some incorrections
    # x_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/x_' + stage + '/'
    # y_data_dir = '/home/gcx/lstm_sequences/entire_dataset/' + stage + '/y_' + stage + '/'
    # corrected version of dataset
    # x_data_dir = '/home/gcx/datasets/labelling/' + stage + '/x_' + stage + '/'
    # y_data_dir = '/home/gcx/datasets/labelling/' + stage + '/label_' + stage + '/'

    while True:
        # x = np.zeros((12, 12, 256), dtype=np.float32)

        # y = np.zeros((4,1), dtype=np.float32)

        i = np.random.randint(0, len(file_list))

        current_file = os.path.split(file_list[i])[1]

        # if channel_number == 256:  # If we consider prediction maps

        # temp_img = np.asanyarray(cv2.imread(x_data_dir + current_file_list[j * temporal_stride]), np.float32)
        file_name = dataset_folder + '/features/' + current_file

        try:
            # print('file_name', file_name)
            x = np.reshape(np.load(file_name), (img_size, img_size, numb_channels)) / 255.
            # temp_img = np.asanyarray(
            #                     cv2.cvtColor(
            #                         cv2.imread(file_name), cv2.COLOR_BGR2GRAY),
            #             np.float32)/ 255.
            # print(temp_img.shape)

        except:
            print('could not load image')
            print(file_name)
            sys.error('cloud not load image"')
            break

        # Load data for Y
        file_name = dataset_folder + '/gt/' + current_file

        try:
            gt = np.load(file_name)

            y = np.reshape(gt, 5)
            y = y[4]
        except:
            print('file name: ', file_name)
            sys.error('could not load ground truth')

        # print(x.max())
        # print(y.max())

        yield (np.asanyarray(x, np.float32), np.asanyarray(y, np.float32))


def group_by_batch(dataset, batch_size):
    while True:
        # try:
            # sources, targets, weights = zip(*[next(dataset) for i in xrange(batch_size)])
            # batch = (np.stack(sources), np.stack(targets), np.stack(weights))
        sources, targets = zip(*[next(dataset) for i in xrange(batch_size)])
        batch = (np.stack(sources), np.stack(targets))
        yield batch
        # except:
        #     print('error on grouping routine')
        #     return


def group_by_batch_with_names(dataset, batch_size):
    while True:
        # try:
            # sources, targets, weights = zip(*[next(dataset) for i in xrange(batch_size)])
            # batch = (np.stack(sources), np.stack(targets), np.stack(weights))
        sources, targets, names = zip(*[next(dataset) for i in xrange(batch_size)])
        batch = (np.stack(sources), np.stack(targets))
        yield batch
        # except:
        #     print('error on grouping routine')
        #     return


def load_dataset(directory, batch_size, n_frames, img_size, reduced_size, stage, channel_number, gt_time_steps):
    # print('directory + stage + x_:', directory + 'multi_channel_prediction/')
    print('directory + stage + x_:', directory + 'x_' + stage)
    print('directory + stage + y_:', directory + 'y_' + stage)

    # frames_list = np.genfromtxt(stage + '_dataset_' + str(n_frames) + '.txt', dtype='str')
    frames_list = np.genfromtxt(stage + '_dataset_20.txt', dtype='str')

    # numpy_files = list_numpys(directory + stage + '/x_' + stage)
    # y_files = list_pictures(directory + stage + 'y_' + stage)

    generator = lstm_image_generator(frames_list,
                                     n_frames=n_frames,
                                     img_size=img_size,
                                     reduced_size=reduced_size,
                                     channel_number=channel_number,
                                     stage=str(stage),
                                     gt_time_steps = gt_time_steps)
    generator = group_by_batch(generator, batch_size)
    return generator


def load_dataset_dt_aug(directory, batch_size, n_frames, img_size, reduced_size, stage, channel_number, gt_time_steps):
    # print('directory + stage + x_:', directory + 'multi_channel_prediction/')
    print('directory + stage + x_:', directory + 'x_' + stage)
    print('directory + stage + y_:', directory + 'y_' + stage)

    # frames_list = np.genfromtxt(stage + '_dataset_' + str(n_frames) + '.txt', dtype='str')
    # path that was used only for positive examples
    # frames_list = np.genfromtxt(stage + '_dataset_20.txt', dtype='str')
    frames_list = np.genfromtxt('pos_seq/' + stage + '_dataset_40.txt', dtype='str')

    # path that was used only for positive examples
    # frames_list = np.genfromtxt('neg_n_pos_seq/comb_' + stage + '_dataset_20.txt', dtype='str')

    # numpy_files = list_numpys(directory + stage + '/x_' + stage)
    # y_files = list_pictures(directory + stage + 'y_' + stage)

    generator = lstm_image_generator_dt_aug(frames_list,
                                     n_frames=n_frames,
                                     img_size=img_size,
                                     reduced_size=reduced_size,
                                     channel_number=channel_number,
                                     stage=str(stage),
                                     gt_time_steps = gt_time_steps)
    generator = group_by_batch(generator, batch_size)
    return generator


def load_dataset_strided(directory, batch_size, n_frames, img_size, reduced_size, stage, channel_number, gt_time_steps, n_frames_txt_file):

    # frames_list = np.genfromtxt(stage + '_dataset_' + str(n_frames) + '.txt', dtype='str')
    # path that was used only for positive examples
    # frames_list = np.genfromtxt(stage + '_dataset_20.txt', dtype='str')
    # frames_list = np.genfromtxt('pos_seq/' + stage + '_dataset_40.txt', dtype='str')
    # n_frames_txt_file = 40
    frames_list = np.genfromtxt('neg_n_pos_seq/correct_' + stage + '_dataset_3_' + str(n_frames_txt_file) + '.txt', dtype='str')
    # frames_list = np.genfromtxt('neg_n_pos_seq/correct_' + stage + '_regress_' + str(n_frames) + '.txt', dtype='str')

    # path that was used only for positive examples
    # frames_list = np.genfromtxt('neg_n_pos_seq/comb_' + stage + '_dataset_20.txt', dtype='str')

    # numpy_files = list_numpys(directory + stage + '/x_' + stage)
    # y_files = list_pictures(directory + stage + 'y_' + stage)

    generator = lstm_image_generator_strided(frames_list, directory,
                                            n_frames=n_frames,
                                            img_size=img_size,
                                            reduced_size=reduced_size,
                                            channel_number=channel_number,
                                            stage=str(stage),
                                            gt_time_steps = gt_time_steps)
    generator = group_by_batch(generator, batch_size)
    return generator


def load_dataset_strided_flatten(directory, batch_size, n_frames, img_size, reduced_size, stage, channel_number, gt_time_steps, n_frames_txt_file):

    # frames_list = np.genfromtxt(stage + '_dataset_' + str(n_frames) + '.txt', dtype='str')
    # path that was used only for positive examples
    # frames_list = np.genfromtxt(stage + '_dataset_20.txt', dtype='str')
    # frames_list = np.genfromtxt('pos_seq/' + stage + '_dataset_40.txt', dtype='str')
    n_frames_txt_file = 40
    frames_list = np.genfromtxt('neg_n_pos_seq/correct_' + stage + '_dataset_3_' + str(n_frames_txt_file) + '.txt', dtype='str')
    # frames_list = np.genfromtxt('neg_n_pos_seq/correct_' + stage + '_regress_' + str(n_frames) + '.txt', dtype='str')

    # path that was used only for positive examples
    # frames_list = np.genfromtxt('neg_n_pos_seq/comb_' + stage + '_dataset_20.txt', dtype='str')

    # numpy_files = list_numpys(directory + stage + '/x_' + stage)
    # y_files = list_pictures(directory + stage + 'y_' + stage)

    generator = lstm_image_generator_strided_flatten(frames_list, directory,
                                            n_frames=n_frames,
                                            img_size=img_size,
                                            reduced_size=reduced_size,
                                            channel_number=channel_number,
                                            stage=str(stage),
                                            gt_time_steps = gt_time_steps)
    generator = group_by_batch(generator, batch_size)
    return generator


def load_dataset_strided_flatten_reshaped(directory, batch_size, n_frames,
                                          img_size, reduced_size, stage,
                                          channel_number, gt_time_steps, n_frames_txt_file):

    # frames_list = np.genfromtxt(stage + '_dataset_' + str(n_frames) + '.txt', dtype='str')
    # path that was used only for positive examples
    # frames_list = np.genfromtxt(stage + '_dataset_20.txt', dtype='str')
    # frames_list = np.genfromtxt('pos_seq/' + stage + '_dataset_40.txt', dtype='str')
    n_frames_txt_file = 40
    frames_list = np.genfromtxt('neg_n_pos_seq/correct_' + stage + '_dataset_3_' + str(n_frames_txt_file) + '.txt',
                                dtype='str')
    # frames_list = np.genfromtxt('neg_n_pos_seq/correct_' + stage + '_regress_' + str(n_frames) + '.txt', dtype='str')

    # path that was used only for positive examples
    # frames_list = np.genfromtxt('neg_n_pos_seq/comb_' + stage + '_dataset_20.txt', dtype='str')

    # numpy_files = list_numpys(directory + stage + '/x_' + stage)
    # y_files = list_pictures(directory + stage + 'y_' + stage)

    generator = lstm_image_generator_strided_flatten_reshaped(frames_list,
                                                              directory,
                                                              n_frames=n_frames,
                                                              img_size=img_size,
                                                              reduced_size=reduced_size,
                                                              channel_number=channel_number,
                                                              stage=str(stage),
                                                              gt_time_steps = gt_time_steps)
    generator = group_by_batch(generator, batch_size)
    return generator


def load_dataset_cropped_regressor(directory, batch_size, img_size, stage, numb_channels):
    # this load_dataset_cropped_regressor is suposed to be used in the
    # refinning step to get more acurate bounding boxes

    if stage == 'train':
        delete_list = []
        file_list = glob.glob('/home/gcx/datasets/refinment/regressor/features/*.npy')
        print(len(file_list))
        for file in enumerate(file_list):
            gt = np.load('/home/gcx/datasets/refinment/regressor/gt/' + os.path.split(file[1])[1])
            if np.prod(gt == np.zeros((1, 5))):
                # delete this entry from file list
                delete_list.append(file[0])
                # del file_list[file[0]]
        pass
        # for delete_entry in enumerate(delete_list):
        # delete_list
        file_list = [i for j, i in enumerate(file_list) if j not in delete_list]
        pass

    elif stage == 'test':
        delete_list = []
        file_list = glob.glob('/home/gcx/datasets/refinment/regressor_test/features/*.npy')
        print(len(file_list))

        for file in enumerate(file_list):
            gt = np.load('/home/gcx/datasets/refinment/regressor_test/gt/' + os.path.split(file[1])[1])
            if np.prod(gt == np.zeros((1, 5))):
                #delete this entry from file list
                delete_list.append(file[0])
                pass
        file_list = [i for j, i in enumerate(file_list) if j not in delete_list]

    print(len(file_list))

    generator = lstm_image_generator_cropped_regressor(file_list, dataset_folder=directory, img_size=img_size, numb_channels=numb_channels)

    generator = group_by_batch(generator, batch_size)
    return generator


def load_dataset_not_distorted(directory, batch_size, n_frames, img_size, reduced_size, stage, channel_number, gt_time_steps):
    # print('directory + stage + x_:', directory + 'multi_channel_prediction/')
    print('directory + stage + x_:', directory + 'x_' + stage)
    print('directory + stage + y_:', directory + 'y_' + stage)

    # frames_list = np.genfromtxt(stage + '_dataset_' + str(n_frames) + '.txt', dtype='str')
    # path that was used only for positive examples
    # frames_list = np.genfromtxt(stage + '_dataset_20.txt', dtype='str')
    # frames_list = np.genfromtxt('pos_seq/' + stage + '_dataset_40.txt', dtype='str')

    frames_list = np.genfromtxt('neg_n_pos_seq/correct_' + stage + '_dataset_' + str(n_frames) + '.txt', dtype='str')

    # path that was used only for positive examples
    # frames_list = np.genfromtxt('neg_n_pos_seq/comb_' + stage + '_dataset_20.txt', dtype='str')

    # numpy_files = list_numpys(directory + stage + '/x_' + stage)
    # y_files = list_pictures(directory + stage + 'y_' + stage)

    generator = lstm_image_generator_not_distorted(frames_list,
                                                   n_frames=n_frames,
                                                   img_size=img_size,
                                                   reduced_size=reduced_size,
                                                   channel_number=channel_number,
                                                   stage=str(stage),
                                                   gt_time_steps = gt_time_steps)

    # generator = group_by_batch(generator, batch_size)
    generator = group_by_batch_with_names(generator, batch_size)

    return generator


def regressor_dataset_strided(directory, batch_size, n_frames, img_size, reduced_size, stage, channel_number, gt_time_steps):
    # print('directory + stage + x_:', directory + 'multi_channel_prediction/')
    print('directory + stage + x_:', directory + 'x_' + stage)
    print('directory + stage + y_:', directory + 'y_' + stage)

    # frames_list = np.genfromtxt(stage + '_dataset_' + str(n_frames) + '.txt', dtype='str')
    # path that was used only for positive examples
    # frames_list = np.genfromtxt(stage + '_dataset_20.txt', dtype='str')
    # frames_list = np.genfromtxt('pos_seq/' + stage + '_dataset_40.txt', dtype='str')

    frames_list = np.genfromtxt('neg_n_pos_seq/correct_' + stage + '_regress_' + str(n_frames) + '.txt', dtype='str')

    # path that was used only for positive examples
    # frames_list = np.genfromtxt('neg_n_pos_seq/comb_' + stage + '_dataset_20.txt', dtype='str')

    # numpy_files = list_numpys(directory + stage + '/x_' + stage)
    # y_files = list_pictures(directory + stage + 'y_' + stage)

    generator = lstm_img_gen_regressor(frames_list,
                                       n_frames=n_frames,
                                       img_size=img_size,
                                       reduced_size=reduced_size,
                                       channel_number=channel_number,
                                       stage=str(stage),
                                       gt_time_steps = gt_time_steps)
    generator = group_by_batch(generator, batch_size)
    return generator

